;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.connection.RosNode
  (:gen-class
    :implements [org.ros.node.NodeMain])
  (:require [clojure.string :as str]
            [clojure.core.async :refer [>! >!! <! <!! go go-loop chan buffer close! thread alts! alts!! timeout]]
            [kop.config :as cfg]
            [kop.statevars :as svs])
  (:import org.ros.namespace.GraphName
           org.ros.message.MessageListener
           [org.ros.internal.message Message]
           [org.opencv.core Core CvType Mat MatOfByte]
           (org.opencv.imgproc Imgproc)
           (org.opencv.imgcodecs Imgcodecs)
           (nu.pattern OpenCV)
           [java.util ArrayList]))

; die Zeile stoert hier etwas, aber muss aufgerufen werden, bevor irgendwas mit OpenCV passiert
(OpenCV/loadShared)

(defn -getDefaultNodeName [_]
  (GraphName/of "clojure/node"))

(defn message-to-hashmap [message]
  (let [fields (-> message .toRawMessage .getFields vec)]
    (zipmap (map (comp keyword #(.getName %)) fields)
            (map (fn [field]
                   (let [val (.getValue field)]
                     (cond (instance? ArrayList val)
                           (map message-to-hashmap (vec val))

                           (instance? Message val)
                           (message-to-hashmap val)

                           :else
                           val)))
                 fields))))

(def ros-cv-types*
  {"bgr8"         CvType/CV_8UC3
   "mono8"        CvType/CV_8UC1
   "rgb8"         CvType/CV_8UC3
   "mono16"       CvType/CV_16UC1
   "bgr16"        CvType/CV_16UC3
   "rgb16"        CvType/CV_16UC3
   "bgra8"        CvType/CV_8UC4
   "rgba8"        CvType/CV_8UC4
   "bgra16"       CvType/CV_16UC4
   "rgba16"       CvType/CV_16UC4
   "bayer_rggb8"  CvType/CV_8UC1
   "bayer_bggr8"  CvType/CV_8UC1
   "bayer_gbrg8"  CvType/CV_8UC1
   "bayer_grbg8"  CvType/CV_8UC1
   "bayer_rggb16" CvType/CV_16UC1
   "bayer_bggr16" CvType/CV_16UC1
   "bayer_gbrg16" CvType/CV_16UC1
   "bayer_grbg16" CvType/CV_16UC1
   "yuv422"       CvType/CV_8UC2
   "16UC1"        CvType/CV_8UC2 ;CV_16UC1
   })


(defn message-to-opencv-image [message]
  (let [array-offset (-> message .getData .arrayOffset)
        imageInBytes (-> message .getData .array vec (subvec array-offset) byte-array) 
        encoding (-> message .getEncoding)
        cvimage (Mat. (.getHeight message) (.getWidth message) (get ros-cv-types* encoding))]
    (.put cvimage 0 0 imageInBytes)
    cvimage))

; Tiefenbilder sind so ausgefiltert, dass Verarbeitungszeit unter 1 Sekunde liegt
(let [original-width 480
      original-height 360
      clip-width 480
      clip-height 240
      offset-width 0
      offset-height 120
      slice-max (dec clip-width)
      row-distance 2 ;3
      column-distance 2 ;3
      bytes-per-value 2
      value-distance-in-row (* column-distance bytes-per-value)
      slice-width (* bytes-per-value clip-width)
      original-width-in-bytes (* bytes-per-value original-width)
      max-width-per-slice (dec slice-width)
      first-slice-offset (+ (* bytes-per-value original-width 
                               (+ offset-height (/ (- original-height clip-height offset-height) 2))) 
                            (* bytes-per-value 
                               (+ offset-width (/ (- original-width clip-width offset-width) 2))))]
                         
  (defn get-depth-matrix [slicedChannelBuffer-of-bytes]
      (loop [row 0
             slice-offset first-slice-offset
             rows []]
        (if (< row clip-height)
          (let [slice (.slice slicedChannelBuffer-of-bytes slice-offset slice-width)
                slice-values (loop [ii 0
                                    row-matrix []]
                               (if (< ii max-width-per-slice)
                                 (recur (+ value-distance-in-row ii) (conj row-matrix (.getUnsignedShort slice ii)))
                                 row-matrix))]
            (recur (+ row-distance row) (+ slice-offset original-width-in-bytes) (conj rows slice-values)))
          (vec (reduce concat rows)))))
)              
   

(def ros-config
  (cfg/make-config [:depth :image] (fn [_] true)))


(defn subscribe-depth [connectednode]
  (let [subscriber-depth (.newSubscriber connectednode "/camera/depth/image_raw" "sensor_msgs/Image")
        last-msg-time-depth (atom 0)]
    (.addMessageListener subscriber-depth
                         (proxy [MessageListener] []
                           (onNewMessage [message]
                             (let [now (System/currentTimeMillis)]
                               (when (>= (- now @last-msg-time-depth) 1000)
                                 ; Verarbeitung der Tiefenbilder ist so langsam, dass ich nicht viel drosseln muss, aber so kommen sie regelmaessiger
                                 (svs/reset-sv-all :depth {:data (get-depth-matrix (.getData message)) :timestamp now})
                                 (reset! last-msg-time-depth now))))))))



(defn subscribe-image [connectednode]
  (let [subscriber-image (.newSubscriber connectednode "/camera/rgb/image_raw" "sensor_msgs/Image")
        last-msg-time-image (atom 0)]
    (.addMessageListener subscriber-image
                         (proxy [MessageListener] []
                           (onNewMessage [message]
                             ;(println "new image message: " (System/currentTimeMillis))
                             (let [now (System/currentTimeMillis)]
                               (when (>= (- now @last-msg-time-image) 1000) ; Datenrate von ROS drosseln (ROS-Daten kommen mit 30 fps)
                                 ;(println "processing new image message: " now)
                                 (svs/reset-sv-all :image {:image (message-to-opencv-image message) :timestamp now})
                                 (reset! last-msg-time-image now))))))))

(defn -onStart [thisnode connectednode]
  (when (cfg/get-config ros-config :depth)
    (subscribe-depth connectednode))
  (when (cfg/get-config ros-config :image)
    (subscribe-image connectednode)))


(defn -onShutdown [thisnode connectednode])

(defn -onShutdownComplete [thisnode connectednode])

(defn -onError [thisnode connectednode throwable]
  (println "ROS error: " throwable))
