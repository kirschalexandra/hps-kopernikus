;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.


(ns kop.decision-modules
  (:require [clojure.math.numeric-tower :as math]
            [hps.heuristics :refer :all]
            [hps.types :refer :all]
            [hps.decision-modules :refer :all]
            [hps.utilities.misc :as utmisc]
            [hps.library.reconf :as rc]
            [kop.statevars :refer :all :as svs]))

(defn scale-linearly [minval maxval mintarget maxtarget value]
  (let [scaled (int (+ mintarget (* (- value minval) (/ (- maxtarget mintarget) (- maxval minval)))))
        in-bounds (max mintarget (min maxtarget scaled))]
    (if (< (math/abs in-bounds) 5)
      0 in-bounds)))


(def-heuristic user-rc-command
               :producer (def-expert-fun []
                                         (let [{:keys [rc-forward rc-turn] :as user-cmd} (svv :rc-percept)]
                                           (when-not (nil? user-cmd)
                                             {:trans rc-forward
                                              :rot   rc-turn}))))


(def-heuristic kopi-command
               :producer (def-expert-fun []
                           (:value (svv :dm-kopi-executed-decision))))


                                              
(def-heuristic refuse-behavior
               :producer (def-expert-fun [params]
                           {:base-command
                            ;(condp > (mod (:duration params) 1500)
                            (condp > (:duration params)
                              ;0 {:trans 0 :rot 0}
                              ;20 {:trans 0 :rot 30}
                              0 {:trans 0 :rot 30}
                              500 {:trans 0 :rot -30}
                              1000 {:trans 0 :rot 30}
                              1500 {:trans 0 :rot 0})
                            :sound-command
                            (when (:juststarted params)
                              "/home/kopernikus/hps/hps-kopernikus/resources/r2d2_sounds/1.mp3")
                            :block-other-behaviors (< (:duration params) (:length-exclusive params))}))

(def-heuristic animation-refuse-cmd
               :producer (def-expert-fun []
                           (:base-command (:value (svv :dm-animation-refuse-executed-decision)))))

(def-heuristic animation-refuse-sound
               :producer (def-expert-fun []
                           (when-let [soundfile (:sound-command (:value (svv :dm-animation-refuse-executed-decision)))]
                             {:soundfile soundfile}))) 

(def-heuristic accept-all
               :evaluator (def-expert-fun [alts]
                                          (utmisc/map-to-kv (fn [alt] 1.0) alts)))

; compromise macht momentan überhaupt keinen Sinn: eigentlich sollte sich Kommando daraus ergeben, welches sicher ist und zusätzlich müsste man ein Kompromisskommando (aus den Durchschnittswerten oder so) bauen
; diese Funktionalität ist leider gerade nicht möglich
(def-heuristic compromise
               :evaluator (def-expert-fun [alts]
                                          (utmisc/map-to-kv (fn [alt] (rand 1.0)) alts))) ; Nicht wundern: Werte werden hinterher noch normalisiert, deswegen bekommt immer einer 0.5 und einer 0.0


;; Decision modules                                          


; default behavior
(declare-dm :kopi
            :available-experts {:producers  [:user-rc-command]
                                :evaluators [:accept-all]}
            :decision-parameters {:default-decision  (map->DecisionResult {:decision {:trans 0 :rot 0}})
                                  :run-frequency-fun (run-frequency-fun-by-time 200)})

(svs/add-dm-statevars :kopi 'kop.decision-modules)


;; animation

(def command-discrepancy-discount-factor 0.3)

(svs/define-derived-statevar :command-discrepancy {:current-diff {:trans 0 :rot 0} :accumulated-diff {:trans 0 :rot 0}} :dm-kopi-executed-decision
  (fn [_key _ref _old-value {{:keys [trans rot] :as new-value} :value}]
    (when (and new-value (svv :rc-percept))
      (let [{:keys [rc-forward rc-turn]} (svv :rc-percept)
            {old-diff-trans :trans old-diff-rot :rot} (:current-diff (svv :command-discrepancy))
            {new-diff-trans :trans new-diff-rot :rot :as new-diff} {:trans (math/abs (- trans rc-forward)) :rot (math/abs (- rot rc-turn))}]
        (reset-sv-val :command-discrepancy {:accumulated-diff {:trans (+ new-diff-trans (* command-discrepancy-discount-factor old-diff-trans))
                                                               :rot (+ new-diff-rot (* command-discrepancy-discount-factor old-diff-rot))}
                                             :current-diff new-diff})))))

(def refuse-animation
  {;:onset-condition #(> (:trans (:accumulated-value (svv :command-discrepancy))) 0.2)
   :onset-condition #(and (:enc-left (svv :encoders)) (> (:enc-left (svv :encoders)) 200) (< (:enc-left (svv :encoders)) 1000))
   ;:stop-condition #(< (:trans (:accumulated-diff (svv :command-discrepancy))) 0.01)
   :stop-condition #(and (:enc-left (svv :encoders)) (> (:enc-left (svv :encoders)) 1000))
   :length-exclusive  1000 ; Zeit wo nur Animation spielt und sonst nichts
   :length-overall 1600
   :length-minimum 1600 ; auch wenn Bedingung wegfällt, soll Animation Mindestlänge haben? -> Problem evtl. dass Kommando nicht auf 0 zurueckgesetzt wird
   :loop? false
   :anim-producer :refuse-behavior})

(def empty-config (map->DecisionSetup {:experts {} :parameters {:timer nil}}))

(defn make-animation-configure-fun [animation] 
  (fn [dm]
    (if (number? (:timer (:parameters @(:decision-setup dm)))) ; animation already running
    
      (let [timer-val (:timer (:parameters @(:decision-setup dm)))
            duration (- (System/currentTimeMillis) timer-val)]
       
        (if (or ; stop condition met?
                (and ((:stop-condition animation))
                     (> duration (:length-minimum animation)))
                ; animation over and no looping? -> Animationsproducer muss selbst modulo rechnen, wenn loop gewollt ist und Animation mehrmals durchläuft!
                (and (> duration (:length-overall animation))
                     (not (:loop? animation))))
          empty-config ; -> switch to normal mode
          ; keep on running animation, check whether to mix behaviors
          (map->DecisionSetup 
            {:experts {:producers [[(:anim-producer animation) :timer timer-val :duration duration :juststarted false :length-exclusive (:length-exclusive refuse-animation)]]
                       :evaluators [:accept-all]}
             :parameters {:timer timer-val}})))

      ; else: animation not running yet
      (if ((:onset-condition animation)) ; condition for animation met?
        (map->DecisionSetup 
          {:experts {:producers [[(:anim-producer animation) :timer (System/currentTimeMillis) :duration 0 :juststarted true :length-exclusive (:length-exclusive refuse-animation)]]
                     :evaluators [:accept-all]}
           :parameters {:timer (System/currentTimeMillis)}})
        ; else: nothing to do
        empty-config))))



(declare-dm :animation-refuse
            :available-experts {:producers  [:refuse-behavior]
                                :evaluators [:accept-all]}
            :configure-fun (make-animation-configure-fun refuse-animation)
            :decision-parameters {:default-decision  (map->DecisionResult {:decision {:base-command nil :sound-command nil :block-other-behaviors false}})
                                  :run-frequency-fun (run-frequency-fun-by-time 200)
                                  ;:timer nil ; starting time of animation
                                  :max-iterations-per-step 1})


(svs/add-dm-statevars :animation-refuse 'kop.decision-modules)
                          


;; base-command: combining default and animation

(declare-dm :base-command
            :available-experts {:producers  [:kopi-command :animation-refuse-cmd]
                                :evaluators [:accept-all :compromise]}
            :configure-fun (fn [_]
                             (if (:block-other-behaviors (:value (svv :dm-animation-refuse-executed-decision)))
                               (map->DecisionSetup {:experts {:producers [:animation-refuse-cmd] :evaluators [:accept-all]}})
                               (map->DecisionSetup {:experts {:producers [:animation-refuse-cmd :kopi-command] :evaluators [:compromise]}}))) ; hier müssten noch weitere Experten rein, die z.B. ein Durchschnittskommando ausrechnen
            :decision-parameters {:default-decision  (map->DecisionResult {:decision {:trans 0 :rot 0}})
                                  :run-frequency-fun (run-frequency-fun-by-time 200)}) 

(svs/add-dm-statevars :base-command 'kop.decision-modules)


;; sound: currently only used by animation

(declare-dm :sound
            :available-experts {:producers  [:animation-refuse-sound]
                                :evaluators [:accept-all]}
            :decision-parameters {:default-decision  (map->DecisionResult {:decision {:trans 0 :rot 0}})
                                  :run-frequency-fun (run-frequency-fun-by-time 200)}) 

(svs/add-dm-statevars :sound 'kop.decision-modules)


