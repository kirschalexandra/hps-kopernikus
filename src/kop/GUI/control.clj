;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.GUI.control
  (:require [clojure.java.io :as io]
            [clojure.set :as set]
            [hps.utilities.gui :as gut]
            [hps.decision-modules :as dms]
            [kop.GUI.menu :as menu]
            [kop.traces :as tc]
            [kop.statevars :as svs]
            [kop.run :as run])
  (:import (javafx.scene.control Label Button TextField ChoiceBox CheckBox Slider Dialog ButtonType Alert Alert$AlertType)
           (javafx.scene.layout VBox HBox GridPane TilePane BorderPane FlowPane Priority Pane Region)
           (javafx.beans.property ReadOnlyStringWrapper ReadOnlyObjectWrapper)
           (javafx.beans.value ChangeListener ObservableValue)
           (javafx.event Event EventHandler)
           (javafx.stage FileChooser FileChooser$ExtensionFilter)
           (javafx.scene.image Image ImageView)
           (javafx.geometry Orientation)
           (javafx.scene.input KeyEvent KeyCode)))

; ------------------------------------------------------- ;
; -- Buttons -------------------------------------------- ;
; ------------------------------------------------------- ;

(defn make-control-button [img]
  (let [btn (Button. "" (ImageView. (Image. (io/input-stream (io/resource (str "icons/" img ".gif"))))))]
    (gut/add-style btn "control-button")
    btn))

(defn make-button-box [& buttons]
  (let [button-box (TilePane. Orientation/HORIZONTAL)]
    (.add (.getStyleClass button-box) "kopi-control-button-box")
    (apply gut/add-to-container button-box buttons)
    button-box))

(def playing? (atom false))
(def recording? (atom false))

(defmulti add-button-functionality (fn [x _] x))


(defmethod add-button-functionality :play [_ play-button]
  (gut/set-button-action play-button
                         (if (run/trace-loaded?)
                           (let [no-dms-in-trace (empty? (tc/dms-in-trace))]
                             (when no-dms-in-trace (run/start-decision-modules))
                             (let [replay-future (tc/start-sv-replay)]
                               (reset! playing? true)
                               (future
                                 (deref replay-future)
                                 (reset! playing? false);)))
                                 (when no-dms-in-trace (run/stop-decision-modules)))))
                           (let [alert (Alert. Alert$AlertType/WARNING
                                               "Please connect load a trace first."
                                               (into-array [ButtonType/CLOSE]))]
                             (-> alert .getDialogPane (.setMinHeight Region/USE_PREF_SIZE))
                             (.showAndWait alert))))
                           
  (add-watch playing? :mark-button
             (fn [_key _ref _old-value new-value]
               (.setStyle play-button (format "-fx-background-color: %s;" (if new-value "#707070;" "-darkblue")))))
)


(defmethod add-button-functionality :record [_ record-button]
  (gut/set-button-action record-button
                         (reset! recording? true)
                         (tc/start-sv-logging))
  (add-watch recording? :mark-button
             (fn [_key _ref _old-value new-value]
               (.setStyle record-button (format "-fx-background-color: %s;" (if new-value "#707070" "-darkblue")))))
  )

(defmethod add-button-functionality :stop [_ stop-button]
  (gut/set-button-action stop-button
                         (when @recording?
                           (let [tracefile (tc/stop-sv-logging)]
                             (gut/show-alert-message
                               (str "Trace saved to " (.toString tracefile) ". You can open it by pressing Alt+O.")
                               "Information"
                               "Trace saved"))
                           (reset! recording? false))
                         (when @playing?
                           (tc/stop-sv-replay)
                           (reset! playing? false)
                           (run/stop-decision-modules)))

  (.setStyle stop-button "-fx-background-color: #707070;")                       

  (add-watch playing? :mark-stop-button
             (fn [_key _ref _old-value new-value]
               (.setStyle stop-button (format "-fx-background-color: %s;" (if new-value "-darkblue" "#707070")))))

  (add-watch recording? :mark-stop-button
             (fn [_key _ref _old-value new-value]
               (.setStyle stop-button (format "-fx-background-color: %s;" (if new-value "-darkblue""#707070")))))
)

; ------------------------------------------------------- ;
; -- Display source of current trace -------------------- ;
; ------------------------------------------------------- ;

(defn make-trace-source-view []
  (let [box (VBox.)
        trace-label (Label. "Trace Source: ")
        source-field (Label.)]
    (gut/add-to-container box trace-label source-field)
    (.add (.getStyleClass trace-label) "caption")
    (.setStyle source-field "-fx-max-width: 300; -fx-text-overrun: leading-ellipsis;")
    (.setStyle box "-fx-alignment: top-left;")

    (add-watch tc/trace-source :show-trace-source
               (fn [_key _ref _old-value new-value]
                 (.setText source-field new-value)))

    (add-watch tc/trace-saved? :show-trace-saved
               (fn [_key _ref _old-value new-value]
                 (.setStyle source-field (format "%s-fx-text-fill: %s;" (.getStyle source-field) (if new-value "black" "#7F7F7F")))))

    box))

; ------------------------------------------------------- ;
; -- Update timesteps with new trace -------------------- ;
; ------------------------------------------------------- ;

(defn get-neighbouring-timestep-from-list [timesteps min-ts next-or-prev ts]
  (loop [remaining-timesteps timesteps
         last-ts (case next-or-prev :next 0 :prev min-ts)]
    (if (empty? remaining-timesteps)
      ts
      (if ((case next-or-prev :next > :prev >=) (first remaining-timesteps) ts)
        (case next-or-prev
          :next (first remaining-timesteps)
          :prev last-ts)
        (recur (rest remaining-timesteps) (first remaining-timesteps))))))


; ------------------------------------------------------- ;
; -- Box for jumping in and cutting trace --------------- ;
; ------------------------------------------------------- ;
(declare cut-trace)

(defn make-cut-buttons [timestep-field]
  (let [box (HBox.)
        start-cut-box (HBox.)
        stop-cut-box (HBox.)
        start-cut-button (Button. "[")
        stop-cut-button (Button. "]")
        start-cut-field (Label. "")
        stop-cut-field (Label. "")
        cut-string-to-time (fn [field] (tc/get-global-absolute-timestep (* 1000 (read-string (.getText field)))))]
    (gut/add-to-container start-cut-box start-cut-button start-cut-field)
    (gut/add-to-container stop-cut-box stop-cut-field stop-cut-button)
    (gut/add-to-container box start-cut-box stop-cut-box)
    (HBox/setHgrow start-cut-box Priority/ALWAYS)
    (HBox/setHgrow stop-cut-box Priority/ALWAYS)

    (.setStyle start-cut-box "-fx-alignment: center-left; -fx-spacing: 5;")
    (.setStyle stop-cut-box "-fx-alignment: center-right; -fx-spacing: 5;")

    (gut/set-button-action start-cut-button
                           (.setText start-cut-field (.getText timestep-field)))

    (gut/set-button-action stop-cut-button
                           (.setText stop-cut-field (.getText timestep-field)))

    (defn cut-trace []
      (tc/cut-trace! (cut-string-to-time start-cut-field) (cut-string-to-time stop-cut-field))
      (.setText start-cut-field "")
      (.setText stop-cut-field ""))

    box))


(defn make-trace-elements []
  (let [box (VBox.)
        slider-box (VBox.)
        step-slider (Slider.)
        timestep-field (Label.)
        cut-elements (make-cut-buttons timestep-field)
        step-button (make-control-button "next")
        prev-button (make-control-button "previous")
        reset-button (make-control-button "reset")
        cut-button (make-control-button "cut")]
    ;(HBox/setHgrow timestep-field Priority/ALWAYS)
    (gut/add-to-container slider-box step-slider timestep-field)
    (gut/add-to-container box cut-elements slider-box (make-button-box reset-button prev-button step-button cut-button))
    (.add (.getStyleClass box) "container")
    (.setStyle slider-box "-fx-alignment: center;")
    (.setId step-slider "timeslider")
    (.setDisable box true)
    
    (add-watch tc/trace-meta :adapt-gui
               (fn [_key _ref _old-value new-trace]
                 (let [[min-timestep max-timestep] (tc/process-timesteps)]
                   (.setMin step-slider min-timestep)
                   (.setMax step-slider max-timestep)
                   (.setDisable box false)
                   (.setValue step-slider min-timestep)

                   (gut/set-button-action reset-button (.setValue step-slider min-timestep)))))
    
    (add-watch run/connected-to-robot :adapt-gui
               (fn [_key _ref _old-value new-value]
                 (when new-value
                   (.setDisable box true))))

    (gut/set-button-action step-button (.setValue step-slider (tc/get-global-next-timestep (.getValue step-slider))))
    (.setId step-button "step-button")
    (gut/set-button-action prev-button (.setValue step-slider (tc/get-global-previous-timestep (.getValue step-slider))))

    (.addListener (.valueProperty step-slider)
                  (proxy [ChangeListener] []
                    (^void changed [^ObservableValue ov ^Number oldval ^Number newval]
                      (.setText timestep-field (format "%.3f s" (/ (tc/get-global-relative-timestep newval) 1000)))
                      (tc/set-state-for-timestep (long newval)))))

    (gut/set-button-action cut-button (cut-trace))

    box))

; ------------------------------------------------------- ;
; -- Box for playback and record ------------------------ ;
; ------------------------------------------------------- ;

(defn make-connection-elements []
  (let [outer-box (VBox.)
        inner-box (HBox.)
        source-field-box (make-trace-source-view)
        play-button (make-control-button "play")
        record-button (make-control-button "record")
        stop-button (make-control-button "stop")
        connected-field (Button. "")]

    (.setGraphic connected-field (ImageView. (Image. (io/input-stream (io/resource (str "icons/disconnected.png"))))))
    (gut/add-to-container inner-box connected-field (make-button-box record-button play-button stop-button))
    (gut/add-to-container outer-box source-field-box inner-box)
    ;(.add (.getStyleClass inner-box) "container")
    (.add (.getStyleClass outer-box) "container")
    (VBox/setVgrow source-field-box Priority/ALWAYS)
  
    (gut/set-button-action connected-field
                           (if @run/connected-to-robot
                             (menu/disconnect-robot) ; use functions from menu instead of run to get warnings in GUI if something goes wrong
                             (menu/connect-robot)))

    (add-watch run/connected-to-robot :show-connected
               (fn [_key _ref _old-value new-value]
                 (let [file (str "icons/" (if new-value "" "dis") "connected.png")]
                   (.setGraphic connected-field (ImageView. (Image. (io/input-stream (io/resource file))))))))
 
    (doseq [[function button] [[:play play-button] [:record record-button] [:stop stop-button]]]
      (add-button-functionality function button))

    outer-box))

; ------------------------------------------------------- ;
; -- Bring it all together ------------------------------ ;
; ------------------------------------------------------- ;

(defn make-info-and-control-box []
  (let [control-elements-box (HBox.)]
    (gut/add-to-container control-elements-box (make-connection-elements) (make-trace-elements))
    (.add (.getStyleClass control-elements-box) "kopi-control-elements-box")
    control-elements-box))
  
