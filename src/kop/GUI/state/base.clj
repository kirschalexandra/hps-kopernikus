;; Copyright 2018
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.GUI.state.base
  (:require [clojure.java.io :as io]
            [hps.utilities.gui :as gut])
  (:import (javafx.scene.control Label Button TextField ChoiceBox CheckBox Slider Dialog ButtonType ButtonBar$ButtonData ProgressBar TreeTableColumn$CellDataFeatures Tooltip TabPane Tab TitledPane)
           (javafx.scene.layout VBox HBox GridPane TilePane BorderPane FlowPane Priority Pane Background BackgroundFill CornerRadii)
           (javafx.beans.value ChangeListener ObservableValue)
           (javafx.scene.image Image ImageView)
           (javafx.geometry Orientation Insets)
           (javafx.scene.paint Color)))

(def motor-fields [:rc-turn :rc-forward :rc-turn-raw :rc-forward-raw :rc-speed-scale-raw :rc-speed-scale])
(def projector-fields [:rc-projector-pos :rc-projector-pos-raw])

(defn make-raw-percepts-display
  ([fields] (make-raw-percepts-display fields "rcpercepts-value-"))
  ([fields sv-prefix]
   (let [grid (GridPane.)]
     (doseq [[fieldname row-number] (map list fields (range (count fields)))]
       (let [valuefield (Label.)]
         (.add grid (Label. (str (name fieldname) ": ")) 0 row-number)
         (.add grid valuefield 1 row-number)
         (.setId valuefield (str sv-prefix (name fieldname)))))
     grid)))

(defn make-command-arrows []
  (let [up-down-box (VBox.)
        right-left-box (HBox.)
        [up down left right :as direction-labels]
        (map #(Label. "" (ImageView. (Image. (io/input-stream (io/resource (str "icons/" % ".png"))))))
             ["go-forward" "go-backward" "turn-left" "turn-right"])]
    (gut/add-to-container up-down-box up down)
    (gut/add-to-container right-left-box left up-down-box right)
    (.setStyle up-down-box "-fx-spacing: 20;")
    (.setStyle right-left-box "-fx-alignment: center-left; -fx-spacing: 5;")
    [right-left-box
     (fn [upval downval leftval rightval]
       (doseq [[label val] [[up upval] [down downval] [left leftval] [right rightval]]]
         (.setBackground label (Background. (into-array [(BackgroundFill. (Color/web "#29425b" (/ val 128)) (CornerRadii. 20.0) (Insets. -3.0))])))))
     ]))

(defn make-user-input-display []
  (let [box (HBox.)
        [cmd-arrow-box update-fun] (make-command-arrows)]
    (gut/add-to-container box
                          cmd-arrow-box
                          (make-raw-percepts-display motor-fields))
    (.add (.getStyleClass box) "container")

    (defn update-user-input-display [{:keys [rc-turn rc-forward]}]
      (when (and rc-turn rc-forward)
        (update-fun
          (if (> rc-forward 0) rc-forward 0)
          (if (< rc-forward 0) (- rc-forward) 0)
          (if (< rc-turn 0) (- rc-turn) 0)
          (if (> rc-turn 0) rc-turn 0))))

    box))

(defn make-command-display []
  (let [box (HBox.)
        [cmd-arrow-box update-fun] (make-command-arrows)]
    (gut/add-to-container box
                          cmd-arrow-box
                          (make-raw-percepts-display [:trans :rot] "base-command-"))
    (.add (.getStyleClass box) "container")

    (defn update-command-display [{:keys [trans rot]}]
      (when (and trans rot)
        (update-fun
          (if (> trans 0) trans 0)
          (if (< trans 0) (- trans) 0)
          (if (< rot 0) (- rot) 0)
          (if (> rot 0) rot 0))))

    box))

(defn make-projector-display []
  (make-raw-percepts-display projector-fields))

(defn make-motors-display []
  (let [box (VBox.)
        panes (map (fn [[title content-fun]] (TitledPane. title (content-fun)))
                   [["User command" make-user-input-display]
                    ["Robot command" make-command-display]
                    ["Projector" make-projector-display]])]
    (apply gut/add-to-container box panes)
    box))