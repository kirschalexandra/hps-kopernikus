;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.GUI.menu
  (:require [clojure.java.io :as io]
            [clj-time.core :as time]
            [clj-time.format :as timef]
            [hps.utilities.gui :as gut]
            [kop.GUI.decision-modules :as gdm]
            [kop.traces :as tc]
            [kop.run :as run]
            [kop.GUI.config :as kfg])
  (:import (javafx.scene.control Menu MenuItem ButtonType Alert Alert$AlertType)
           (javafx.stage DirectoryChooser)
           (javafx.scene.layout Region)))




(defn make-connect-disconnect-items [menufile]
  (let [menuitemconnect (MenuItem. "Connect _Robot")
        menuitemdisconnect (MenuItem. "Disconnect _Robot")]
    (doseq [[item accelerator] [[menuitemconnect {:key "R" :modifier :ctrl}]
                                [menuitemdisconnect {:key "R" :modifier :alt}]]]
      (.setAccelerator item (gut/make-keycode accelerator))
      (.add (.getItems menufile) item))

    (defn connect-robot []
      (let [connected? (run/connect-robot)]
        (if connected?
          (do
            (.setDisable menuitemdisconnect false)
            (.setDisable menuitemconnect true))
          (gut/show-warning "Connection to robot not possible."))))

    (defn disconnect-robot []
      (let [disconnected? (run/disconnect-robot)]
        (if disconnected?
          (do
            (.setDisable menuitemdisconnect true)
            (.setDisable menuitemconnect false))
          (gut/show-warning "Disconnect failed."))))

    (gut/set-button-action menuitemconnect (connect-robot))
    (gut/set-button-action menuitemdisconnect (disconnect-robot))
    (.setDisable menuitemdisconnect true)))

(defn get-trace-directory [scene]
  (let [chooser (DirectoryChooser.)
        kopilogdir (io/file (System/getenv "KOPERNIKUS_LOG_DIR"))]
    (.setTitle chooser "Choose trace directory")
    (when kopilogdir
      (.setInitialDirectory chooser kopilogdir))
    (.showDialog chooser (.getWindow scene))))

(defn no-log-dir [scene alert-text]
  (let [alert (Alert. Alert$AlertType/WARNING
                      alert-text
                      (into-array [ButtonType/CLOSE]))]
    (-> alert .getDialogPane (.setMinHeight Region/USE_PREF_SIZE))
    (.showAndWait alert))
  (get-trace-directory scene))


(defn get-default-trace-directory [scene]
  (let [defaultlogdir (io/file (System/getenv "KOPERNIKUS_DEFAULT_LOG_DIR"))]
    (cond (nil? defaultlogdir)
          (no-log-dir scene "No default log directory configured. To do so set system variable KOPERNIKUS_DEFAULT_LOG_DIR.")

          (not (.exists defaultlogdir))
          (no-log-dir scene (format "Default log dir %s does not exist." (.getName defaultlogdir)))

          :else
          (io/file defaultlogdir))))


(defn get-last-trace-directory [scene]
  (let [log-base-dir (clojure.java.io/file (System/getenv "KOPERNIKUS_LOG_DIR"))
        last-dir (->> (map (fn [dirname]
                             (try
                               (timef/parse (timef/formatters :date-hour-minute-second) dirname)
                               (catch IllegalArgumentException _)))
                           (vec (.list log-base-dir)))
                      (filter (comp not nil?))
                      (sort time/after?)
                      first)]
    (if last-dir
      (io/file log-base-dir (timef/unparse (timef/formatters :date-hour-minute-second) last-dir))
      (no-log-dir scene "No last trace found."))))

(defn call-trace-connect [scene get-dir-fun]
  (let [tracedir (get-dir-fun scene)]
    (when tracedir
      (run/load-trace tracedir))))

(defn save-trace [tracedir]
  (let [trace @tc/trace]
    (if (empty? trace)
      (let [alert (Alert. Alert$AlertType/WARNING
                          "There is no trace to save."
                          (into-array [ButtonType/CLOSE]))]
        (-> alert .getDialogPane (.setMinHeight Region/USE_PREF_SIZE))
        (.showAndWait alert))
      (if tracedir
        (tc/save-sv-trace tracedir)
        (tc/save-sv-trace)))))
                             
                             
                             
(defn adjust-menu-for-kopi [menubar scene]
  (let [menufile (first (filter #(= (.getText %) "File") (.getMenus menubar)))
        menuconfigure (Menu. "_Configure")
        menuitemquit (first (.getItems menufile))]

    (.add (.getMenus menubar) menuconfigure)
    (.clear (.getItems menufile))                           ; erstmal File Menü leeren

    (make-connect-disconnect-items menufile)
    (gut/add-menu-separator menufile)
    (gut/create-menu-item menufile "_Open ..." {:key "O" :modifier :ctrl} (partial call-trace-connect scene get-trace-directory))
    (gut/create-menu-item menufile "Open _Last" {:key "O" :modifier :alt} (partial call-trace-connect scene get-last-trace-directory))
    (gut/create-menu-item menufile "Open _Default" "CTRL+ALT+O" (partial call-trace-connect scene get-default-trace-directory))
    (gut/create-menu-item menufile "Open _Default" "CTRL+L" (partial call-trace-connect scene (fn [_] (io/file @tc/trace-source))))
    (gut/add-menu-separator menufile)
    (gut/create-menu-item menufile "_Save" {:key "S" :modifier :ctrl} (fn [] (save-trace @tc/trace-source)))
    (gut/create-menu-item menufile "Save _As ..." {:key "S" :modifier :alt} (fn [] (save-trace (get-trace-directory scene))))
    ;(gut/create-menu-item menufile "Save As De_fault" "CTRL+ALT+S" (fn [] (save-trace (get-default-trace-directory scene)))) ; ist mir zu gefährlich etwas zu überschreiben, was man nicht will -> default einfach als symlink auf den entsprechenden Datensatz setzen
    (gut/add-menu-separator menufile)

    (gut/create-menu-item menuconfigure "State_var display" {:key "V" :modifier :alt} kfg/open-state-display-configs)
    (gut/create-menu-item menuconfigure "_Connection" {:key "C" :modifier :alt} kfg/open-connection-configs)

    (.add (.getItems menufile) menuitemquit)                ; quit wieder hinzufügen

    menubar))
