;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.GUI.state
  (:require [clojure.java.io :as io]
            [hps.GUI.make-decision :as hpsgui]
            [hps.utilities.gui :as gut]
            [hps.decision-modules :as dms]
            [kop.GUI.state.encoders :as kenc]
            [kop.GUI.state.base :as kbs]
            [kop.GUI.state.camera :as kcam]
            [kop.config :as cfg]
            [kop.statevars :as svs])
  (:import (javafx.scene.control Label Button TextField ChoiceBox CheckBox Slider Dialog ButtonType ButtonBar$ButtonData ProgressBar TreeTableColumn$CellDataFeatures Tooltip TabPane Tab TitledPane)
           (javafx.scene.layout VBox HBox GridPane TilePane BorderPane FlowPane Priority Pane Background BackgroundFill CornerRadii)
           (javafx.beans.value ChangeListener ObservableValue)
           (javafx.scene.image Image ImageView)
           (javafx.geometry Orientation Insets)
           (javafx.scene.paint Color)))

(def state-display-config
  (cfg/make-config (keys (svs/get-all-statevars)) (fn [_] true)))


(defn initialize-state-region [stateRegion]
  (let [state-tabs (TabPane.)
        tabs (map (fn [[title content-fun id]]
                    (let [tab (Tab. title (content-fun))]
                      (.setId tab id)
                      tab))
                  [["Motors" kbs/make-motors-display "motors"]
                   ["Encoders" kenc/make-encoder-display "encoders"]
                   ;["Microphone" (fn [] (Label. "not yet implemented")) "microphone"]
                   ["Depth Camera" kcam/make-depth-camera-display "depth-camera"]
                   ["Camera" kcam/make-color-camera-display "rgb-camera"]])]
    (.setId state-tabs "state-tabs")
    (.addAll (.getTabs state-tabs) (into-array tabs))
    (gut/add-to-container stateRegion state-tabs)))

(defn get-tab [scene tabname]
  (let [tab-pane (.lookup scene "#state-tabs")]
    (some #(when (= (.getId %) tabname) %) (vec (.getTabs tab-pane)))))

(def sv-update-funs                                         ; aufpassen: die Werte sind Funktionen, die die Scene als Argument erhalten und sie geben eine Funktion für watch zurück!
  {:encoders           (fn [_]
                         (fn [_key _ref _old-value new-value]
                           (gut/run-in-fx-thread (kenc/set-encoder-values new-value))))
   :encoder-trajectory (fn [_]
                         (fn [_key _ref old-value new-value]
                           (when (and (not-empty (:data new-value))
                                      (not (= (:data old-value) (:data new-value))))
                             (gut/run-in-fx-thread (kenc/draw-encoder-trajectory (:data new-value))))))
   :rc-percept         (fn [scene]
                         (let [fields [:rc-turn :rc-forward :rc-projector-pos-raw :rc-projector-pos :rc-turn-raw :rc-forward-raw :rc-speed-scale-raw :rc-speed-scale]]
                           (fn [_key _ref _old-value new-value]
                             (doseq [fieldname fields]
                               (gut/run-in-fx-thread
                                 (.setText (.lookup scene (str "#rcpercepts-value-" (name fieldname)))
                                           (str (fieldname new-value)))))
                             (kbs/update-user-input-display new-value))))
   :base-command       (fn [scene]
                         (fn [_key _ref _old-value new-value]
                           (doseq [fieldname [:trans :rot]]
                             (gut/run-in-fx-thread
                               (.setText (.lookup scene (str "#base-command-" (name fieldname)))
                                         (str (fieldname new-value)))))
                           (kbs/update-command-display new-value)))
   :depth              (fn [scene]
                         (fn [_key _ref old-value new-value]
                           ;(let [tab (get-tab scene "depth-camera")]
                             (when new-value ;(and new-value (or (not tab) (.isSelected tab)))
                               (gut/run-in-fx-thread (kcam/display-depth-image new-value)))))
   :image              (fn [scene]
                         (fn [_key _ref _old-value new-value]
                           ;(let [tab (get-tab scene "rgb-camera")]
                             (when new-value ;(and new-value (or (not tab) (.isSelected tab))) ; (not (= new-value old-value))
                                (gut/run-in-fx-thread (kcam/display-color-image new-value)))))})

(def sv-reset-funs
  {:encoders           (fn [_] (gut/run-in-fx-thread (kenc/reset-encoder-values)))
   :encoder-trajectory (fn [_] (gut/run-in-fx-thread(kenc/reset-encoder-trajectory-display)))
   :rc-percept         (fn [scene]
                         (doseq [fieldname [:rc-turn :rc-forward :rc-projector-pos-raw :rc-projector-pos :rc-turn-raw :rc-forward-raw :rc-speed-scale-raw :rc-speed-scale]]
                           (gut/run-in-fx-thread
                             (.setText (.lookup scene (str "#rcpercepts-value-" (name fieldname))) "")))
                         (kbs/update-user-input-display {:rc-turn 0 :rc-forward 0}))
   :base-command       (fn [scene]
                         (doseq [fieldname [:trans :rot]]
                           (gut/run-in-fx-thread
                             (.setText (.lookup scene (str "#base-command-" (name fieldname))) "")))
                         (kbs/update-command-display {:trans 0 :rot 0}))
   :depth              (fn [_] (gut/run-in-fx-thread (kcam/reset-depth-image)))
   :image              (fn [_] (gut/run-in-fx-thread (kcam/reset-color-image)))})

(defn display-state [scene]
  (doseq [[sv show?] (cfg/get-config state-display-config)]
    (when (and show? (sv sv-update-funs))
      (add-watch (svs/get-sv-atom sv) :display ((sv sv-update-funs) scene))))

  (add-watch state-display-config nil
             (fn [_key _ref old-value new-value]
               (let [[switch-on switch-off] (loop [[[sv newval] & restvals] (vec new-value)
                                                   [on off :as result] [[] []]]
                                              (if sv
                                                (cond (and newval (not (sv old-value))) ; voher false, jetzt true -> switch on
                                                      (recur restvals [(conj on sv) off])

                                                      (and (not newval) (sv old-value)) ; vorher true, jetzt false -> switch off
                                                      (recur restvals [on (conj off sv)])

                                                      :else (recur restvals result))
                                                result))]
                 (doseq [sv switch-on]
                   (when (sv sv-update-funs)
                     (add-watch (svs/get-sv-atom sv) :display ((sv sv-update-funs) scene))))
                 (doseq [sv switch-off]
                   (remove-watch (svs/get-sv-atom sv) :display)
                   (when (sv sv-reset-funs)
                     ((sv sv-reset-funs) scene)))))))
