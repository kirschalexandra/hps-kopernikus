;; Copyright 2018
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.GUI.config
  (:require [hps.utilities.gui :as gut]
            [hps.utilities.misc :as mut]
            [kop.config :as cfg]
            [kop.connection :as conn]
            [kop.GUI.state :as kst])
  (:import (javafx.scene.layout StackPane GridPane VBox HBox)
           (javafx.scene.control Label Button CheckBox)
           (javafx.scene Scene)
           (javafx.stage Stage)))


(defn make-generic-config-window [config title]
  (let [box (VBox.)
        button-box (HBox.)
        button-ok (Button. "Ok")
        button-cancel (Button. "Cancel")
        checkboxes (into {} (map (fn [[sv on?]]
                                   (let [cb (CheckBox. (name sv))]
                                     (.setSelected cb on?)
                                     [sv cb]))
                                 (cfg/get-config config)))
        state-config-scene (Scene. box)
        state-config-window (Stage.)]

    (apply gut/add-to-container box (vals checkboxes))
    (gut/add-to-container box button-box)
    (gut/add-to-container button-box button-ok button-cancel)
    (.setStyle box "-fx-padding: 10; -fx-spacing: 10;")
    (.setStyle button-box "-fx-alignment: center; -fx-spacing: 10;")

    (gut/set-button-action button-ok
                           (cfg/set-config config
                                           (mut/kv-map #(.isSelected %) checkboxes))
                           (.close state-config-window))

    (gut/set-button-action button-cancel (.close state-config-window))

    (doto state-config-window
      (.setTitle title)
      (.setAlwaysOnTop true)
      (.setScene state-config-scene)
      (.show))))

(def open-state-display-configs (partial make-generic-config-window kst/state-display-config "Configure Display of State Variables"))

(def open-connection-configs (partial make-generic-config-window conn/connection-config "Configure Connections"))
