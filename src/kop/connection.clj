;; Copyright 2018
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.connection
  (:require [dynne.sampled-sound :as dynne]
            [kop.config :as cfg]
            [kop.connection.base :as base]
            [kop.connection.RosNode :as rosnode]
            [kop.connection.ros :as ros]
            [kop.statevars :as svs]))

(def connection-config
  (cfg/make-config [:base :ros-depth :ros-image] (fn [_] true)))

; TODO: Platz fuer diesen Code suchen (in start-connection ist zu spaet, wahrscheinlich muss ich Verhalten bei base noch aendern)
(add-watch connection-config nil
             (fn [_key _ref
                  {old-base :base old-ros-depth :ros-depth old-ros-image :ros-image}
                  {new-base :base new-ros-depth :ros-depth new-ros-image :ros-image}]
               ;(when-not (= old-base new-base)
                 ;(if new-base
                   ;(base/start-base-connection)
                   ;(base/stop-base-connection)))
               (cfg/set-config rosnode/ros-config {:depth new-ros-depth :image new-ros-image})))

(defn start-connection [] 
  (cond (or (cfg/get-config connection-config :ros-depth) 
            (cfg/get-config connection-config :ros-image))
        (let [roscore-proc (.exec (Runtime/getRuntime) "roscore")]
          (Thread/sleep 500)
          ; use ROS startup time for establishing base connection
          (when (cfg/get-config connection-config :base)
            (base/start-base-connection))
          ; start camera ROS process
          (let [camera-proc (.exec (Runtime/getRuntime) "roslaunch realsense_camera r200_nodelet_rgbd.launch")]
            (defn stop-ros-components []
              (.destroy roscore-proc)
              (.destroy camera-proc)))
          (ros/start-ros))
          
        (cfg/get-config connection-config :base)
        (base/start-base-connection))
  ; execute sound commands
  (add-watch (svs/get-sv-atom :sound-command) :execute-sound
    (fn [_key _ref _old-value new-value]
      (when new-value
            (dynne/play (dynne/read-sound (:soundfile new-value))))))
  )


(defn stop-connection []
  (when (cfg/get-config connection-config :base)
    (base/stop)
    (base/stop-base-connection))
  (when (or (cfg/get-config connection-config :ros-depth) 
            (cfg/get-config connection-config :ros-image))
    (ros/stop-ros)
    (stop-ros-components))
  (remove-watch (svs/get-sv-atom :sound-command) :execute-sound))

(def stop base/stop)

