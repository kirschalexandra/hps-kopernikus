;; Copyright 2018
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.


;;; Idee run: eine Datei, wo man sieht, was aktiv ist, also wo SV Werte herkommen (vom Roboter oder von Trace) und ob Decision Modules aktiv sind
;;; dabei darauf achten, dass es keine Konflikte gibt (z.B. Roboter verbunden und Trace geladen) und sinnvolle defaults gesetzt sind (z.B. ob derived statevars berechnet werden oder aus Logdatei kommen)

(ns kop.run
  (:require [clojure.string :as s]
            [kop.connection :as conn]
            [kop.statevars :as svs]
            [kop.traces :as tc]
            ;[kop.decision-modules :as dms]
            [hps.decision-modules :as hdms]
            [hps.utilities.misc :as mut]))
            
            
(declare disconnect-robot)
(declare connected-to-robot)            
            
;; decision modules

(def active-dms (atom []))

(defn start-decision-modules
  ([] (start-decision-modules [:kopi])) ;(hdms/list-decision-modules 'kop.decision-modules))) ;[:kopi]))
  ([dms]
   (doseq [dm-name dms]
     (hdms/start-dm dm-name 'kop.decision-modules)
     (swap! active-dms conj dm-name))))

(defn stop-decision-modules [] 
  (doseq [dm-name @active-dms]
    (hdms/stop-dm dm-name 'kop.decision-modules)
    (swap! active-dms (fn [vec elem] (remove #(= elem %) vec)) dm-name)))
    

;; wiring
(defn wire-dm-to-command [dm-name command-sv]
  (let [dm (hdms/get-dm-from-name dm-name 'kop.decision-modules)]
    (add-watch (:executed-decision dm) :decision-watch
      (fn [_key _ref _old-value new-value]
        (svs/reset-sv-val command-sv (:value new-value))))))
        
(defn wire-sv-to-command [source-sv target-sv transform-fun]        
  (add-watch (svs/get-sv-atom source-sv) :decision-watch
    (fn [_key _ref _old-value new-value]
      (svs/reset-sv-val target-sv (transform-fun new-value)))))


;; traces -> just use functionality in kop.traces

(def trace-loaded? tc/trace-loaded?)

(defn load-trace [tracedir]
  (when @connected-to-robot (disconnect-robot))
  (tc/read-trace tracedir) ; derived SVs: ist problematisch bei Zeitsprüngen, aber ist praktisch wenn man neue derived SVs testen will
  (let [unlogged-derived-statevars (remove #(contains? @tc/trace-meta %) (svs/get-derived-svs))]
    ;(println "unlogged derived svs: " unlogged-derived-statevars)
    (svs/start-sv-derivations unlogged-derived-statevars))
  (let [dms-from-log (tc/dms-in-trace)] ;(distinct (remove nil? (map svs/identify-dm-statevar (keys @tc/trace))))]
  
    (defn without-dm-statevar-connection-do [fun]
      (svs/disconnect-dms-from-statevars 'kop.decision-modules)
      (fun)
      (doseq [dm dms-from-log]
        (svs/connect-dm-to-statevars dm 'kop.decision-modules)))

    (svs/disconnect-dms-from-statevars 'kop.decision-modules) ; in case there was a connection from a previous opened file
    (doseq [dm dms-from-log]
      (svs/connect-dm-to-statevars dm 'kop.decision-modules))))


;; robot connection

(def connected-to-robot (atom false)) ; -> als atom nötig, evtl. in connection umziehen?

;(defn connected-to-robot? [])

(defn connect-robot 
  ([] (connect-robot {:use-dms true}))
  ([{:keys [use-dms]}]
   (let [connection-established? (try
                                   (conn/start-connection)
                                   (reset! connected-to-robot true)
                                   true
                                   (catch Exception _ (do (println "connection failed") false)))]
     (when connection-established?
       (when (trace-loaded?) (tc/reset-trace))
       (svs/reset-derived-svs)
       (svs/start-sv-derivations)
       (if use-dms
         (do 
           (start-decision-modules)
           (wire-dm-to-command :kopi :base-command))
           ;(wire-dm-to-command :base-command :base-command)
           ;(wire-dm-to-command :sound :sound-command))
         (wire-sv-to-command :rc-percept :base-command
                             (fn [new-rc-percept] 
                               {:trans (:rc-forward new-rc-percept) :rot (:rc-turn new-rc-percept)})))
       ; Projektorkommando wird momentan immer durchgereicht
       (wire-sv-to-command :rc-percept :projector-command
                             (fn [new-rc-percept] 
                               {:pos (:rc-projector-pos new-rc-percept)})))
                               
     connection-established?)))

   
(defn disconnect-robot []
  (stop-decision-modules)
  (conn/stop)
  (try
    (conn/stop-connection)
    (catch Exception _ (do (println "no connection to be disconnected") false)))
  (reset! connected-to-robot false)
  true)
    


;; state variables

(defn activate-sv-derivations [])

(defn deactivate-sv-derivations [])
