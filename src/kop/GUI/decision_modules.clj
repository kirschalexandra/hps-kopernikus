;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.GUI.decision-modules
  (:require [hps.GUI.structure :as guistruct]
            [hps.GUI.make-decision :as hpsgui]
            [hps.utilities.gui :as gut]
            [hps.decision-modules :as dms]
            [kop.run :as run])
  (:import (javafx.scene.control Label Button TextField ChoiceBox CheckBox Slider Dialog ButtonType ButtonBar$ButtonData ProgressBar TreeTableColumn$CellDataFeatures Tooltip TabPane Tab)
           (javafx.scene.layout VBox HBox GridPane TilePane BorderPane FlowPane Priority Pane)))

(defn make-decision-module-tabs [tab-pane decision-modules]
  (doseq [dm-keyword decision-modules]
    (let [tab (Tab. (name dm-keyword))
          tabbox (BorderPane.)
          tabcontentpane (guistruct/make-decision-module-basic-display)
          button-box (HBox.)
          step-button (Button. "Step Decision Module")
          dm-spec (dms/get-dm-from-name dm-keyword 'kop.decision-modules)
          {:keys [producers evaluators adaptors] :as experts} (:available-experts dm-spec)
          [experts-pane _] (vec (.getItems tabcontentpane))
          [producer-pane adaptor-pane evaluator-pane parameters-pane :as panes] (vec (.getChildren experts-pane))]
      (.setDividerPosition tabcontentpane 0 0.4)
      (gut/add-to-container button-box step-button)
      (.setCenter tabbox tabcontentpane)
      (.setBottom tabbox button-box)
      (.setContent tab tabbox)
      (.add (.getTabs tab-pane) tab)
      (.setId tab (name dm-keyword))

      (.setStyle button-box "-fx-alignment: center; -fx-padding: 10;")
      (gut/set-button-action step-button
                             (run/without-dm-statevar-connection-do (fn [] (dms/step-decision-module dm-spec))))

      (run! (fn [[pane experts]] (hpsgui/initialize-experts-in-display pane experts))
            [[producer-pane producers] [evaluator-pane evaluators] [adaptor-pane adaptors]])
      )))

(defn display-dm-activities [dm-pane dm]
  (let [tabs (-> dm-pane .getTabs)
        dm-tab (some #(when (= (.getId %) (name (:name dm))) %) tabs)
        dm-pane-pure (-> dm-tab .getContent .getCenter)
        [inputs _outputs] (-> dm-pane-pure .getItems vec)]

    ; executed decision: als State (d.h. gesendetes Kommando) anzeigen
    ; könnte man evtl. irgendwann verschönern und z.B. in Tabelle hervorheben

    ;(add-watch (:executed-decision dm) :display
    ;(fn [_key _ref _old-value new-value]
    ;(println "executed decision: " new-value)))
    ;))

    (add-watch (:internal-decision dm) :display
               (fn [_key _ref _old-value new-value]
                 (gut/run-in-fx-thread (hpsgui/update-decision-display new-value dm-pane-pure))))
    ;(println "internal decision: " new-value)))

    (add-watch (:decision-setup dm) :display
               (fn [_key _ref _old-value new-value]
                 (gut/run-in-fx-thread (hpsgui/update-setup-display new-value inputs (comp name :name)))))

    (add-watch (:active dm) :display
               (fn [_key _ref _old-value new-value]
                 (if new-value
                   (.add (.getStyleClass dm-tab) "tab-dm-active")
                   (.removeAll (.getStyleClass dm-tab) (into-array ["tab-dm-active"])))))
    )


  (defn stop-dm-watches []
    (doseq [field [:executed-decision :internal-decision :decision-setup :active]]
      (remove-watch (field dm) :display)))
  )


(defn stop-displaying-dm-activities []
  (dms/stop-dm :kopi 'kop.decision-modules)
  (stop-dm-watches))
