;; Copyright 2018
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.GUI.state.encoders
  (:require [clojure.math.numeric-tower :as math]
            [hps.utilities.gui :as gut]
            [hps.utilities.math :as maut])
  (:import (javafx.scene.control Label Button TextField ChoiceBox CheckBox Slider Dialog ButtonType ButtonBar$ButtonData ProgressBar TreeTableColumn$CellDataFeatures Tooltip TabPane Tab TitledPane)
           (javafx.scene.layout VBox HBox GridPane TilePane BorderPane FlowPane Priority Pane Background BackgroundFill CornerRadii)
           (javafx.beans.value ChangeListener ObservableValue)
           (javafx.scene.image Image ImageView)
           (javafx.geometry Orientation Insets)
           (javafx.scene.canvas Canvas GraphicsContext)
           (javafx.scene.paint Color)))

(defn make-encoder-values-display []
  (let [encoder-box (HBox.)
        lblCptLeft (Label. "left: ")
        lblLeft (Label.)
        lblCptRight (Label. "   right: ")
        lblRight (Label.)]
    (.add (.getStyleClass encoder-box) "container")
    (gut/add-to-container encoder-box lblCptLeft lblLeft lblCptRight lblRight)

    (defn set-encoder-values [{:keys [enc-left enc-right]}]
      (.setText lblLeft (str enc-left))
      (.setText lblRight (str enc-right)))

    (defn reset-encoder-values []
      (.setText lblLeft "")
      (.setText lblRight ""))

    encoder-box))

(def enc-example  [{:enc-right -4959, :enc-left -6395, :timestamp 1513692039776} {:enc-right 0, :enc-left 0, :timestamp 1513692026788} {:enc-right 0, :enc-left 0, :timestamp 1513692026788} {:enc-right -4, :enc-left -2, :timestamp 1513692026991} {:enc-right -29, :enc-left -30, :timestamp 1513692027196} {:enc-right -78, :enc-left -77, :timestamp 1513692027399} {:enc-right -136, :enc-left -136, :timestamp 1513692027603} {:enc-right -200, :enc-left -201, :timestamp 1513692027809} {:enc-right -267, :enc-left -266, :timestamp 1513692028013} {:enc-right -332, :enc-left -332, :timestamp 1513692028216} {:enc-right -397, :enc-left -396, :timestamp 1513692028419} {:enc-right -467, :enc-left -452, :timestamp 1513692028623} {:enc-right -559, :enc-left -488, :timestamp 1513692028826} {:enc-right -655, :enc-left -524, :timestamp 1513692029029} {:enc-right -744, :enc-left -564, :timestamp 1513692029232} {:enc-right -817, :enc-left -622, :timestamp 1513692029435} {:enc-right -885, :enc-left -692, :timestamp 1513692029637} {:enc-right -959, :enc-left -765, :timestamp 1513692029840} {:enc-right -1033, :enc-left -839, :timestamp 1513692030042} {:enc-right -1110, :enc-left -916, :timestamp 1513692030246} {:enc-right -1190, :enc-left -996, :timestamp 1513692030449} {:enc-right -1271, :enc-left -1077, :timestamp 1513692030652} {:enc-right -1345, :enc-left -1166, :timestamp 1513692030854} {:enc-right -1404, :enc-left -1268, :timestamp 1513692031056} {:enc-right -1449, :enc-left -1381, :timestamp 1513692031259} {:enc-right -1491, :enc-left -1498, :timestamp 1513692031462} {:enc-right -1534, :enc-left -1614, :timestamp 1513692031665} {:enc-right -1577, :enc-left -1730, :timestamp 1513692031868} {:enc-right -1644, :enc-left -1822, :timestamp 1513692032071} {:enc-right -1728, :enc-left -1906, :timestamp 1513692032274} {:enc-right -1822, :enc-left -2000, :timestamp 1513692032477} {:enc-right -1918, :enc-left -2096, :timestamp 1513692032679} {:enc-right -2017, :enc-left -2195, :timestamp 1513692032882} {:enc-right -2107, :enc-left -2305, :timestamp 1513692033085} {:enc-right -2179, :enc-left -2430, :timestamp 1513692033288}])

;(def enc-example [{:enc-left -1300, :enc-right -2253, :timestamp 1516282399285} {:enc-left -1300, :enc-right -2253, :timestamp 1516282399467} {:enc-left -1300, :enc-right -2253, :timestamp 1516282399674} {:enc-left -1300, :enc-right -2253, :timestamp 1516282399880} {:enc-left -1300, :enc-right -2253, :timestamp 1516282400087} {:enc-left -1300, :enc-right -2253, :timestamp 1516282400351} {:enc-left -1300, :enc-right -2253, :timestamp 1516282400558} {:enc-left -1300, :enc-right -2253, :timestamp 1516282400763} {:enc-left -1300, :enc-right -2253, :timestamp 1516282400968} {:enc-left -1300, :enc-right -2253, :timestamp 1516282401174} {:enc-left -1300, :enc-right -2253, :timestamp 1516282401378} {:enc-left -1324, :enc-right -2277, :timestamp 1516282401584} {:enc-left -1378, :enc-right -2332, :timestamp 1516282401790} {:enc-left -1455, :enc-right -2408, :timestamp 1516282401996} {:enc-left -1563, :enc-right -2516, :timestamp 1516282402201} {:enc-left -1661, :enc-right -2613, :timestamp 1516282402408} {:enc-left -1755, :enc-right -2709, :timestamp 1516282402614} {:enc-left -1831, :enc-right -2825, :timestamp 1516282402819} {:enc-left -1903, :enc-right -2944, :timestamp 1516282403025} {:enc-left -1977, :enc-right -3062, :timestamp 1516282403231} {:enc-left -2051, :enc-right -3181, :timestamp 1516282403436} {:enc-left -2125, :enc-right -3300, :timestamp 1516282403642} {:enc-left -2199, :enc-right -3420, :timestamp 1516282403847} {:enc-left -2271, :enc-right -3537, :timestamp 1516282404052} {:enc-left -2332, :enc-right -3640, :timestamp 1516282404258} {:enc-left -2364, :enc-right -3686, :timestamp 1516282404466} {:enc-left -2366, :enc-right -3689, :timestamp 1516282404671} {:enc-left -2366, :enc-right -3689, :timestamp 1516282404878} {:enc-left -2366, :enc-right -3689, :timestamp 1516282405084}])

(def wheel-distance-in-ticks (* 29 16.5))                   ; 29 cm, 16.5 ticks per cm (bewusst etwas großzügiger angesetzt wg. Ungenauigkeit)

(defn enc-trajectory-to-path [trajectory]
  (when-let [first-measurement (first trajectory)]
    (loop [[last-left last-right :as last-enc] [(:enc-left first-measurement) (:enc-right first-measurement)]
           last-orientation 0
           path [[0 0]]
           [{:keys [enc-left enc-right] :as this-measurement} & rest-trajectory] (rest trajectory)]

      (if (nil? this-measurement)
        path
        (let [[diff-left diff-right :as diff-enc] [(- enc-left last-left) (- enc-right last-right)]
              dist (maut/average diff-enc)
              [last-x last-y] (last path)
              [next-x next-y :as next-point] [(+ last-x (* dist (Math/cos last-orientation)))
                                              (+ last-y (* dist (Math/sin last-orientation)))]
              delta-enc (- diff-right diff-left)
              new-ori-relative (* 2 (Math/asin (max -1.0 (min 1.0 (/ (/ delta-enc 2) wheel-distance-in-ticks)))))]

          (recur [enc-left enc-right]
                 (+ last-orientation new-ori-relative)      ; hier muss man wahrscheinlich noch was normalisieren
                 (conj path next-point)
                 rest-trajectory)
          )))))


(defn make-encoder-trajectory-display []
  (let [canvas-box (Pane.)
        [width height] [8200 6560]  ; ca. 5 x 4 m
        [min-x min-y max-x max-y] [-4100 -3280 4100 3280]
        [canvas-width canvas-height] [410 328]
        [scale-x scale-y] [(/ canvas-width width) (- (/ canvas-height height))]
        [translate-x translate-y] [(- min-x) (- max-y)]
        canvas (Canvas. canvas-width canvas-height)
        gc (.getGraphicsContext2D canvas)]

    (gut/add-to-container canvas-box canvas)

    (doto gc
      ; set coordinate system
      (.scale scale-x scale-y)
      (.translate translate-x translate-y))

    (defn draw-encoder-trajectory [trajectory]
      (doto gc
        (.clearRect min-x min-y width height)
        (.save)
        (.beginPath)
        (.moveTo 0 0))
      ;(println "trajectory: " trajectory)
      ;(println "path: " (enc-trajectory-to-path trajectory))
      (doseq [[px py] (enc-trajectory-to-path trajectory)]
        (.lineTo gc px py))
      (doto gc
        (.setLineWidth 15)
        (.setStroke Color/BLACK)
        (.stroke)
        (.restore)))

    (defn reset-encoder-trajectory-display []
      (gut/clear-canvas canvas))

    canvas-box))

(defn make-encoder-display []
  (let [box (VBox.)]
    (.setStyle box "-fx-padding: 10; -fx-spacing: 10")
    (gut/add-to-container box (make-encoder-values-display) (make-encoder-trajectory-display))
    box))

