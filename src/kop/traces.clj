;; Copyright 2018
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.traces
  (:require [clojure.java.io :as io]
            [clojure.set :as set]
            [clj-time.format :as timef]
            [clj-time.local :as timel]
            [hps.utilities.misc :as mut]
            [kop.statevars :as svs]
            [kop.hps.traces :as tc])
  (:import (java.io IOException)
           (org.opencv.imgcodecs Imgcodecs)
           (org.opencv.core Mat CvType Scalar)))


(defn start-sv-logging
  ([] (start-sv-logging (svs/get-all-statevars)))
  ([statevars]                                                     
   (let [log-dir (io/file (or (System/getenv "KOPERNIKUS_LOG_DIR")
                              (str (System/getProperty "user.home") "/kopi-log"))
                          (timef/unparse (timef/formatters :date-hour-minute-second) (timel/local-now)))
         images-dir (str (.toString log-dir) "/images/")
         depth-dir (str (.toString log-dir) "/depth/")]
     (io/make-parents (io/file images-dir "x.log"))                  ; create directory (or complete path to directory) 
     (io/make-parents (io/file depth-dir "x.log"))
     (def stop-sv-logging
          (tc/start-sv-logging                                   ; Rückgabewert von start-sv-logging ist eine Funktion, mit der man das Loggen beenden kann
            log-dir
            statevars
            {:image (fn [{:keys [image timestamp] :as statevar}]
                        (let [imgfile (str timestamp ".jpg")]
                          (future (Imgcodecs/imwrite (str images-dir imgfile) (or image (Mat. 640 480 CvType/CV_8UC3 (Scalar. 0.0)))))
                          {:source imgfile :timestamp timestamp}))
             :depth (fn [{:keys [data timestamp] :as statevar}]
                        (let [dpfile (str timestamp ".clj")]
                          (future (spit (str depth-dir dpfile) (or data (take (* 480 360) (repeat 0)))))
                          {:source dpfile :timestamp timestamp}))})))))


(def trace (atom nil))
(def trace-meta (atom nil))
(def trace-source (atom nil))
(def trace-saved? (atom true))

(defn read-trace [log-dir]
  (let [image-dir (str (.toString log-dir) "/images/")
        depth-dir (str (.toString log-dir) "/depth/")]
    (reset! trace (tc/read-sv-log log-dir
                                  {:image (fn [{:keys [source timestamp]}]
                                            {:image         (Imgcodecs/imread (str image-dir source))
                                             :source source
                                             :timestamp     timestamp})
                                   :depth (fn [{:keys [source timestamp]}]
                                            {:data (read-string (slurp (str depth-dir source)))
                                             :source source
                                             :timestamp timestamp})})))
  (reset! trace-meta (tc/read-sv-log-meta log-dir))         ; trace-meta muss nach trace gesetzt werden
  (reset! trace-source (.toString log-dir))
  (reset! trace-saved? true))

(defn reset-trace []
  (reset! trace-meta nil)
  (reset! trace nil)
  (reset! trace-source nil)
  (reset! trace-saved? false))

(defn trace-loaded? []
  (boolean @trace))


(declare get-global-relative-timestep)
(declare get-global-absolute-timestep)
(declare get-global-next-timestep)
(declare get-global-previous-timestep)

(defn process-timesteps []
  (let [timesteps (sort < (distinct (apply concat (vals @trace-meta))))
        min-timestep (first timesteps)]

    (defn get-global-relative-timestep [ts]
      (- ts min-timestep))

    (defn get-global-absolute-timestep [relative-ts]
      (long (+ relative-ts min-timestep)))

    (def get-global-next-timestep (partial tc/get-neighbouring-timestep-from-list timesteps min-timestep :next))
    (def get-global-previous-timestep (partial tc/get-neighbouring-timestep-from-list timesteps min-timestep :prev))
    
    (defn get-sorted-timesteps []
      timesteps)

    [min-timestep (last timesteps)]))


(defn set-state-for-timestep [ts]
  "set statevars to the state in the trace at the given time (from every statevar use last available value)"
  (doseq [[sv-name sv-timesteps] @trace-meta]
    (let [last-available-timestep (tc/get-last-available-value sv-timesteps ts)]
      (svs/reset-sv-all 
        sv-name 
        (when last-available-timestep (get (sv-name @trace) last-available-timestep))))))
    

; replay-state: implemented without set-state-for-timestep to make it more efficient

(defn replay-state []
  (loop [timesteps-per-sv @trace-meta
         last-timestamp (first (get-sorted-timesteps))]
    (let [[next-sv-name next-timestamp] (reduce (fn [[_ tmp-next-sv-time :as tmp] [_ sv-time :as sv]]
                                                  (if (and tmp-next-sv-time sv-time (<= sv-time tmp-next-sv-time))
                                                    sv
                                                    (if tmp-next-sv-time tmp sv)))
                                                (mut/kv-map first timesteps-per-sv))
          next-sv-value (get (next-sv-name @trace) next-timestamp)
          remaining-state (assoc timesteps-per-sv next-sv-name (rest (next-sv-name timesteps-per-sv)))]
      ; sleep to simulate original timing
      (Thread/sleep (- next-timestamp last-timestamp))
      (svs/reset-sv-all next-sv-name next-sv-value)
      (when (some not-empty (vals remaining-state))
        (recur remaining-state next-timestamp)))))

    


(defn start-sv-replay []
  ;(replay-state)
  (println "replay starting")
  (let [replay-future (future (replay-state))]
    (future
      (deref replay-future)
      (println "replay finished"))

    (defn stop-sv-replay []
      (println "replay aborted")
      (future-cancel replay-future))

    replay-future))


(defn copy-or-delete-data-files [old-dir new-dir files]
  (if (= old-dir new-dir)
    ; delete unnecessary files
    (let [old-files (filter #(clojure.string/includes? % ".")
                           (map #(.getName %) (file-seq old-dir)))]
      (doseq [file-to-delete (set/difference (set old-files) (set files))]
        (io/delete-file (io/file new-dir file-to-delete))))
    ; copy necessary files
    (doseq [ff files]
      (io/copy (io/file old-dir ff) (io/file new-dir ff)))))
  


(defn save-sv-trace
  ([] (save-sv-trace @trace-source))
  ([log-dir]
   ; general saving of logs
   (tc/save-sv-trace @trace @trace-meta log-dir
     {:image (fn [sv-data] (dissoc sv-data :image))
      :depth (fn [sv-data] (dissoc sv-data :data))})
   ; application-specifig operations: copy images and depth data
   (let [[old-images-dir old-depth-dir] (vec (map #(io/file (str (.toString @trace-source) %)) ["/images/" "/depth/"]))
         [new-images-dir new-depth-dir] (vec (map #(io/file (str (.toString log-dir) %)) ["/images/" "/depth/"]))]
     (io/make-parents (io/file new-images-dir "x.log"))              
     (io/make-parents (io/file new-depth-dir "x.log"))
     (copy-or-delete-data-files old-images-dir new-images-dir (map :source (vals (:image @trace))))
     (copy-or-delete-data-files old-depth-dir new-depth-dir (map :source (vals (:depth @trace)))))
   ; set atoms to new trace
   (reset! trace-saved? true)
   (reset! trace-source (.toString log-dir))))
   

(defn cut-trace! [start end]
  (let [[new-trace new-meta] (tc/cut-trace @trace @trace-meta start end)]
    (reset! trace new-trace)
    (reset! trace-meta new-meta))
  (reset! trace-saved? false))


(defn dms-in-trace []
  (distinct (remove nil? (map svs/identify-dm-statevar (keys @trace)))))

