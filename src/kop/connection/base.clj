;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.connection.base
  (:require [kop.statevars :refer :all]
            [serial.core :as sc]
            [clojure.string :as str]
            [clojure.data.json :as json])
  (:import [java.io InputStreamReader BufferedReader]))


(defn triml-0 [in-string]
  (if (= 0 (int (first in-string)))
    (triml-0 (subs in-string 1))
    in-string))
    
(defn write-percepts-to-statevars [percept-string]
  (let [[percept-type json-string] (str/split percept-string #":: ")
        percept-map (json/read-str json-string)
        sv (case percept-type 
             "RC" :rc-percept 
             "EN" :encoders)]
    (reset-sv-val sv (into {} (map (fn [[k v]] [(keyword k) v]) percept-map))))) ; convert keys from strings to keywords
    
    
(defn process-comment [base-percept init-finished?]
  (println "received comment: " base-percept)  
  (when (= base-percept "# Waiting for commands...")
    (deliver init-finished? true)))
  
    
(defn process-base-percepts [bufferedreader init-finished?]
  (let [last-update (atom (System/currentTimeMillis))]
    (while true
      (when (.ready bufferedreader)
        (reset! last-update (System/currentTimeMillis))
        (try
          (let [base-percept (triml-0 (.readLine bufferedreader))]
            (if (= (first base-percept) \#)
              (process-comment base-percept init-finished?)
              (write-percepts-to-statevars base-percept)))
          (catch Exception e (str "caught exception: " (.getMessage e)))))
      (when (and (> (- (System/currentTimeMillis) @last-update) 1000)
                 (realized? init-finished?))
        (println "No base percepts arriving, check whether Arduino is running")))))
  
    
(defn start-base-connection []
  (let [port (sc/open "/dev/ttyACM0" :baud-rate 38400)
        bufferedreader (BufferedReader. (InputStreamReader. (:in-stream port)))
        init-finished? (promise)
        reader-process (future (process-base-percepts bufferedreader init-finished?))]
        
    (deref init-finished? 3000 false)
    
    (defn send-command [cmd-string]
      ;(println "send command: " cmd-string)
      (sc/write port (byte-array (map (comp byte int) (format "!%s\r" cmd-string)))))     
    
    (add-watch (get-sv-atom :base-command) :cmd-watch
      (fn [_key _ref old-value {:keys [trans rot]}] 
        ;(println "executing command trans: " trans ", rot: " rot)
        (when-not (or (nil? trans) (nil? rot))
          (send-command (str "setVelocities " trans " " rot)))))
          
    (add-watch (get-sv-atom :projector-command) :cmd-watch
      (fn [_key _ref old-value {:keys [pos]}] 
        (when-not (nil? pos)
          (send-command (str "setProj " pos)))))    
        
    (defn stop-base-connection []
      (future-cancel reader-process)
      (remove-watch (get-sv-atom :base-command) :cmd-watch)
      (sc/close! port))    
      
    [port reader-process]))    
    
    
;; some convenience functions for sending simple commands
(defn reset-encoders []
  (send-command "rstEnc"))
  
(defn stop []
  (send-command "setVel1 0")
  (send-command "setVel2 0"))  
