;; Copyright 2018
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.GUI.state.camera
  (:require [hps.utilities.gui :as gut]
            [kop.statevars :as svs]
            [clojure.pprint :refer :all])
  (:import (javafx.scene.control Label Button TextField ChoiceBox CheckBox Slider Dialog ButtonType ButtonBar$ButtonData ProgressBar TreeTableColumn$CellDataFeatures Tooltip TabPane Tab TitledPane)
           (javafx.scene.layout VBox HBox GridPane TilePane BorderPane FlowPane GridPane Priority Pane Background BackgroundFill CornerRadii)
           (javafx.beans.value ChangeListener ObservableValue)
           (javafx.scene.image Image ImageView WritableImage PixelFormat PixelWriter PixelFormat$Type PixelFormat$ByteRgb)
           (javafx.geometry Orientation Insets)
           (javafx.scene.canvas Canvas GraphicsContext)
           (javafx.scene.paint Color)
           (java.nio IntBuffer ByteBuffer)
           (javafx.event EventHandler)
           (javafx.scene.input MouseEvent)
           (org.opencv.imgcodecs Imgcodecs)
           (org.opencv.core MatOfByte Mat CvType Scalar)
           (org.opencv.imgproc Imgproc)
           (java.io ByteArrayInputStream)))


(defn make-depth-camera-image-display []
  (let [box (VBox.)
        canvas-box (Pane.)
        grid (GridPane.)
        [width height] [240 120] ;[160 120] ;[480 360]
        view-scale-factor 2 ; must be integer
        size (* width height)
        image (WritableImage. width height)
        imageview (ImageView. image)
        pw (.getPixelWriter image)
        pr (.getPixelReader image)
        position-label (Label. "Cursor position:")
        depth-label (Label. "Depth value: ")
        empty-image-buffer (IntBuffer/wrap (int-array (take size (repeat 0))))
        pixel-format (PixelFormat/getIntArgbPreInstance)
        buffer (IntBuffer/allocate size)]
        
    (.setPreserveRatio imageview true)
    (.setFitWidth imageview (* width view-scale-factor))
    (.setStyle box "-fx-alignment: center; -fx-spacing: 5;")
    (.add (.getStyleClass grid) "parameter-grid-pane")


    (gut/add-to-container canvas-box imageview)
    (.add grid (Label. "Cursor position: ") 0 0)
    (.add grid position-label 1 0)
    (.add grid (Label. "Depth: ") 0 1)
    (.add grid depth-label 1 1)
    (gut/add-to-container box canvas-box grid)


    (.setOnMouseMoved canvas-box
                      (proxy [EventHandler] []
                        (handle [^MouseEvent event]
                          (let [x (int (/ (.getX event) view-scale-factor))
                                y (int (/ (.getY event) view-scale-factor))]
                            (when (and (< x width) (< y height))
                              (.setText position-label
                                        (cl-format nil "x: ~5,2f, y: ~5,2f" x y))
                              (when-let [depth-all (:data (svs/svv :depth))]
                                (.setText depth-label
                                          (format "%.2f m (%x)"
                                                  (* 0.001 (nth depth-all (+ (* y width) x)))
                                                  (.getArgb pr x y)
                                                  ))))))))
    [box
     ; display fun
     (fn [depth-sv] 
       (.put buffer (int-array (map #(if (<= % 0) 0xffffffff (bit-or 0xff774400 (bit-shift-right % 4))) (:data depth-sv))))
       ; durch den bit-shift wird zwischen 16cm und ca. 4m skaliert, Werte unter 50cm gibt es sowieso praktisch nicht und größere sind in Innenräumen auch selten
       (.rewind buffer)
       (.setPixels pw 0 0 width height pixel-format buffer width))
     ; reset fun
     (fn []
       (.setPixels pw 0 0 width height pixel-format empty-image-buffer width))]))
                                                  


(defn make-depth-camera-data-display []
  (let [grid (GridPane.)
        count-labels {70 (Label.) 60 (Label.) 50 (Label.)}]

    (.add (.getStyleClass grid) "parameter-grid-pane")

    (loop [[[dist label] & rest-labels] (vec count-labels)
           row-count 0]
      (when dist
        (.add grid (Label. (format "Pixels closer than %d cm: " dist)) 0 row-count)
        (.add grid label 1 row-count)
        (recur rest-labels (inc row-count))))
  
    [grid
     (fn [depth-sv]
       (doseq [[dist label] (vec count-labels)]
         (.setText label (str (count (filter #(and (> % 0) (< % (* dist 10))) (:data depth-sv)))))))
        
     (fn []
       (doseq [[_ label] (vec count-labels)]
         (.setText label "")))]))


(defn make-depth-camera-display []
  (let [box (VBox.)
        [image image-display-fun image-reset-fun] (make-depth-camera-image-display)
        [data  data-display-fun  data-reset-fun]  (make-depth-camera-data-display)]
        
    (gut/add-to-container box image data)
    (.setStyle box "-fx-padding: 10; -fx-spacing: 20;")

    (defn display-depth-image [depth-sv]
      (image-display-fun depth-sv)
      (data-display-fun depth-sv))
      
    (defn reset-depth-image []
      (image-reset-fun)
      (data-reset-fun))
    
    box))


(defn make-color-camera-display []
  (let [canvas-box (Pane.)
        image (WritableImage. 640 480)
        imgview (ImageView. image)
        pw (.getPixelWriter image)
        image-array (byte-array (* 640 480 3)) 
        empty-image (Mat. 640 480 CvType/CV_8UC3 (Scalar. 0.0))]
    (gut/add-to-container canvas-box imgview)
    (.setStyle canvas-box "-fx-alignment: center;")
    (defn display-color-image [image-sv]
      (.get (:image image-sv) 0 0 image-array)
      (.setPixels pw 0 0 640 480 (PixelFormat/getByteRgbInstance) image-array 0 (* 3 640)))
    (defn reset-color-image [] 
      (.get empty-image 0 0 image-array)
      (.setPixels pw 0 0 640 480 (PixelFormat/getByteRgbInstance) image-array 0 (* 3 640)))
    canvas-box))



