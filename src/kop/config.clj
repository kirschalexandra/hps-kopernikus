;; Copyright 2018
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.config)

(defn make-config [value-keys init-fun]
  (atom
    (zipmap value-keys (map init-fun value-keys))))

(defn get-config
  ([config-atom] (get-config config-atom nil))
  ([config-atom key]
    (if key
      (key @config-atom)
      @config-atom)))

(defn set-config [config-atom values-map]
  (swap! config-atom merge values-map))
