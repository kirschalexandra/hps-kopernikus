(defproject hps-kopernikus "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :repositories {"rosjava" "https://github.com/rosjava/rosjava_mvn_repo/raw/indigo"}
                 ;"rosjava-master" "https://github.com/rosjava/rosjava_mvn_repo/tree/master"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/core.async "0.4.474"]
                 [heuristic-problem-solver "1.0.0-SNAPSHOT"]
                 [clj-serial "2.0.4-SNAPSHOT"]
                 [org.clojure/data.json "0.2.6"]
                 [clj-time "0.14.2"]
                 [org.craigandera/dynne "0.4.1"]
                 [org.ros.rosjava_core/rosjava "0.2.1" 
                  :exclusions [org.ros.rosjava_bootstrap/message_generation
                               org.ros.rosjava_messages/rosjava_test_msgs
                               org.ros.rosjava_messages/rosgraph_msgs
                               org.ros.rosjava_messages/geometry_msgs
                               org.ros.rosjava_messages/nav_msgs
                               org.ros.rosjava_messages/tf2_msgs]] 
                 [org.ros.rosjava_bootstrap/message_generation "0.1.11"]
                 [org.ros.rosjava_messages/std_msgs "0.5.8" :exclusions [org.ros.rosjava_bootstrap/message_generation]]
                 [org.ros.rosjava_messages/sensor_msgs "1.10.2" :exclusions [org.ros.rosjava_bootstrap/message_generation]]
                 [org.ros.rosjava_messages/dynamic_reconfigure "1.5.34" :exclusions [org.ros.rosjava_bootstrap/message_generation]]
                 [org.openpnp/opencv "3.2.0-0"]]
                 ;[com.github.rosjava.rosjava_extras/cv_bridge_javacv "0.3.2"]]
  :main kop.core
  :aliases {"kopi-gui" ["run" "-m" "kop.core/start-kopi-gui"]}
  :aot [kop.connection.RosNode])
