;; Copyright 2018
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.hps.traces
  (:require [clojure.java.io :as io]
            [clj-time.format :as timef]
            [clj-time.local :as timel]
            [hps.utilities.misc :as mut])
  (:import (java.io IOException BufferedWriter)))


;; -------------------------------------------------------- ;;
;; -- General --------------------------------------------- ;;
;; -------------------------------------------------------- ;;

(defn get-file-for-sv [dir sv-keyword]
  (str dir "/log/" (name sv-keyword) ".log"))

(defn get-meta-filename-from-dir [dir]
  (io/file dir "timestamps.clj"))

;; -------------------------------------------------------- ;;
;; -- Logging --------------------------------------------- ;;
;; -------------------------------------------------------- ;;

(defn log-sv-start
  ([sv-atom writer] (log-sv-start sv-atom writer nil))
  ([sv-atom writer transform-fun]
   (let [keep-on-logging (atom true)
         timestamps (atom [])
         transform-value (fn [val] (if transform-fun (transform-fun val) val))
         data (transform-value @sv-atom)]
     ; for data that does not change often: use current value with timestamp of start of recording
     (when-not (or (nil? data) (and (sequential? data) (empty? data)) (nil? (:timestamp data)))
       (let [now (System/currentTimeMillis)]
         (.write writer (println-str (pr-str (assoc data :timestamp now))))
         (swap! timestamps conj now)));(assoc @sv-atom :timestamp (System/currentTimeMillis)))))
     (add-watch sv-atom :logging
                (fn [_key _ref _old-value new-value]
                  (when (and new-value (:timestamp new-value))
                    (try
                      (.write writer (println-str (pr-str (transform-value new-value))))
                      (swap! timestamps conj (:timestamp new-value))
                      (catch IOException _)))))
     timestamps)))
                            

(defn log-sv-stop [sv-atom writer]
  (remove-watch sv-atom :logging)
  (.flush writer)
  (.close writer)) 


(defn start-sv-logging
  ([dir svs] (start-sv-logging dir svs {}))
  ([dir svs sv-transform-funs]
   (io/make-parents (io/file dir "log/x.log"))                  ; create directory (or complete path to directory)
   (let [writers (mut/map-to-kv #(BufferedWriter. (io/writer (get-file-for-sv dir %) :append true)) (keys svs))
         timestamp-map (loop [[sv-name & other-sv-names] (vec (keys svs))
                              timestamps {}]
                         (let [timestamp-atom (log-sv-start (get svs sv-name)
                                                            (get writers sv-name)
                                                            (get sv-transform-funs sv-name))
                               new-timestamps (assoc timestamps sv-name timestamp-atom)]
                           (if other-sv-names
                             (recur other-sv-names new-timestamps)
                             new-timestamps)))]
     (fn []
       (println "stop logging")
       (doseq [sv-name (keys svs)]
         (log-sv-stop (get svs sv-name) (get writers sv-name))) 
       (spit (get-meta-filename-from-dir dir) (str (mut/kv-map deref timestamp-map)))
       dir))))

;; -------------------------------------------------------- ;;
;; -- Reading and Replay ---------------------------------- ;;
;; -------------------------------------------------------- ;;

;; -- Meta-Infos (timestamps) ----------------------------- ;;

(defn read-sv-log-meta [log-dir]
  (read-string (slurp (get-meta-filename-from-dir log-dir))))


(defn get-timesteps [timeinfo]
  (sort < (distinct (filter (comp not nil?) (vals timeinfo)))))

              
(defn get-last-available-value [timestamps ts]              ; assumption: timesteps are sorted ascendingly
  (loop [remaining-data timestamps
         last-data nil]
    (if (or (empty? remaining-data) (nil? (first remaining-data)))
      last-data
      (let [timestamp (first remaining-data)]
        (cond (nil? timestamp) nil
              (> timestamp ts) last-data
              :else (recur (rest remaining-data) timestamp))))))              

(defn get-neighbouring-timestep-from-list [timesteps min-ts next-or-prev ts]
  (loop [remaining-timesteps timesteps
         last-ts (case next-or-prev :next 0 :prev min-ts)]
    (if (empty? remaining-timesteps)
      ts
      (if ((case next-or-prev :next > :prev >=) (first remaining-timesteps) ts)
        (case next-or-prev
          :next (first remaining-timesteps)
          :prev last-ts)
        (recur (rest remaining-timesteps) (first remaining-timesteps))))))
        
        
;; -- Full data ------------------------------------------- ;;
; indexed by timestamp

(defn read-sv-log-file
  ([file] (read-sv-log-file file nil))
  ([file transform-fun]
   (let [reader (io/reader file)]
     (loop [logs {}]
       (if (.ready reader)
         (let [data ((or transform-fun identity) (read-string (.readLine reader)))]
           (recur (assoc logs (:timestamp data) data)))
         (do
           (.close reader)
           logs))))))


(defn read-sv-log
  ([log-dir] (read-sv-log log-dir {}))
  ([log-dir sv-transform-funs]
   (let [files (file-seq (io/file log-dir "log/"))
         sv-files (filter #(clojure.string/ends-with? % ".log")
                          (map #(.getName %) files))
         logged-statevars (map (comp keyword #(subs % 0 (- (count %) 4))) sv-files)
         sv-logs (into {} (map (fn [sv file]
                                 [sv (read-sv-log-file (io/file log-dir "log/" file) (get sv-transform-funs sv))])
                               logged-statevars
                               sv-files))]
     sv-logs)))

(defn cut-trace [trace meta-info start end]
  [(mut/kv-map (fn [svdata]
                 (into {} (filter (fn [[ts data]] (and (>= ts start) (<= ts end))) svdata)))
               trace)
   (mut/kv-map (fn [timesteps]
                 (filter (fn [ts] (and (>= ts start) (<= ts end))) timesteps))
               meta-info)])

(defn save-sv-trace
  ([trace meta-info log-dir] (save-sv-trace trace meta-info log-dir {}))
  ([trace meta-info log-dir sv-transform-funs]
   (io/make-parents (io/file log-dir "log/x.log"))               ; man muss eine Datei angeben, damit er das Verzeichnis erstellt
   ; + Bilder/Tiefenbilder kopieren
   (spit (get-meta-filename-from-dir log-dir) meta-info)
   (doseq [[sv-name sv-data] trace]
     (let [writer (io/writer (get-file-for-sv log-dir sv-name) :append false)]
       (loop [[[timestamp datapoint] & rest-data] (vec sv-data)]
         (when datapoint
           (let [data-to-write (if (contains? sv-transform-funs sv-name) ((sv-name sv-transform-funs) datapoint) datapoint)]
             (try
               (.write writer (println-str (pr-str data-to-write)))
               (catch IOException _))
             (recur rest-data))))
       (.close writer)))
   log-dir))
  
          
