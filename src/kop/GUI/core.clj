;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.GUI.core
  (:require [clojure.java.io :as io]
            [hps.utilities.gui :as gut]
            [hps.decision-modules :as hdms]
            kop.decision-modules
            [kop.GUI.state :as state]
            [kop.GUI.decision-modules :as dms]
            [kop.GUI.control :as ctrl]
            [kop.GUI.menu :as menu]
            )
  (:import (javafx.scene.control Label Button TextField ChoiceBox CheckBox Slider Dialog ButtonType ButtonBar$ButtonData ProgressBar TreeTableColumn$CellDataFeatures Tooltip TabPane Tab)
           (javafx.scene.layout VBox HBox GridPane TilePane BorderPane FlowPane Priority Pane)))

  

(defn adjust-gui-for-kopi [scene]
  (.add (.getStylesheets scene) (.toExternalForm (io/resource "css/kopernikus.css")))
  (menu/adjust-menu-for-kopi (.lookup scene "#hps-menubar-left") scene)

  (let [hps-split-pane (.lookup scene "#hps-centerSplitPane")
        controlRegion (.lookup scene "#hps-controlRegion")
        [decisionModules stateRegion] (vec (.getItems hps-split-pane))
        decision-modules-tab-pane (TabPane.)
        info-and-control-box (ctrl/make-info-and-control-box)]
    (.setId decision-modules-tab-pane "dm-tabpane")
    (.setId decisionModules "decision-modules-pane")
    (.setId stateRegion "state-region-pane")
    (.setDividerPosition hps-split-pane 0 0.6)
    (gut/add-to-container controlRegion info-and-control-box)
    (gut/add-to-container decisionModules decision-modules-tab-pane)

    (dms/make-decision-module-tabs decision-modules-tab-pane (hdms/list-decision-modules 'kop.decision-modules))
    (HBox/setHgrow decision-modules-tab-pane Priority/ALWAYS)
    (state/initialize-state-region stateRegion)
    (state/display-state scene)
    (doseq [dm-name (hdms/list-decision-modules 'kop.decision-modules)]
      (dms/display-dm-activities decision-modules-tab-pane (hdms/get-dm-from-name dm-name 'kop.decision-modules)))
))
  
  
