;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.statevars
  (:require [clojure.java.io :as io]
            [clojure.string :as s]
            [clojure.math.numeric-tower :as math]
            [clj-time.format :as timef]
            [clj-time.local :as timel]
            [hps.utilities.misc :as mut]
            [hps.decision-modules :as dms]
            [hps.types :as types]))


(def statevar-categories
  (atom {:percepts [:rc-percept :encoders :depth :image]
         :commands [:base-command :projector-command :sound-command]
         :derived-svs []
         :dm-statevars []}))

(def all-statevars
  (atom {:rc-percept        (atom nil)
         :encoders          (atom nil)
         :depth             (atom nil)
         :image             (atom nil)
         :base-command      (atom nil)
         :projector-command (atom nil)
         :sound-command     (atom nil)}))


(defn get-all-statevars []
  @all-statevars)


(defn get-statevars [category]
  (into {}
    (filter (fn [[k v]] (some #(= k %) (category @statevar-categories)))
            @all-statevars)))
            

;; Functions for all statevars
(defn get-sv-value [sv-name]
  @(sv-name @all-statevars))

(def svv get-sv-value)

(defn get-sv-atom [sv-name]
  (sv-name @all-statevars))

(defn reset-sv-val [sv newval]
  (reset! (get-sv-atom sv) (assoc newval :timestamp (System/currentTimeMillis)))) ; timestamp in milliseconds

(defn reset-sv-all [sv newval]
  (reset! (get-sv-atom sv) newval))

(defn add-statevar [sv-name category default-value]
  (swap! all-statevars assoc sv-name (atom default-value))
  (swap! statevar-categories update-in [category] conj sv-name))


;; DM statevars

(defn prepare-setup-for-saving [{:keys [experts parameters alternatives]}]
  (let [{:keys [producers evaluators adaptors]} experts]
    (types/map->DecisionSetup
      {:experts      {:producers  (map #(dissoc % :fun) producers)
                      :evaluators (map #(dissoc % :fun) evaluators)
                      :adaptors   (map #(dissoc % :fun) adaptors)}
       :parameters   (dissoc parameters :accept-best-alternative-fun :default-decision :run-frequency-fun)
       :alternatives alternatives})))

(defn make-dm-sv-name [dm-name dm-access-point]
  (keyword (format "dm-%s-%s" (name dm-name) (name dm-access-point))))


(defn add-dm-statevars [dm-name dm-ns]
  (let [dm-spec (dms/get-dm-from-name dm-name dm-ns)]
    (doseq [[dm-access-point to-sv-fun] [[:internal-decision identity]
                                         [:executed-decision identity]
                                         [:goal identity]
                                         [:decision-setup prepare-setup-for-saving]]]
      (let [sv-keyword (make-dm-sv-name dm-name dm-access-point)]
        (add-statevar sv-keyword :dm-statevars nil)
        (add-watch (dm-access-point dm-spec) :duplicate-as-sv
                   (fn [_key _ref old-value new-value]
                     (when-not (= old-value new-value)
                       (reset-sv-val sv-keyword (to-sv-fun new-value)))))))))

(defn connect-dm-to-statevars [dm-name dm-ns]
  (let [dm-spec (dms/get-dm-from-name dm-name dm-ns)]
    (doseq [dm-access-point [:internal-decision :executed-decision :goal :decision-setup]]
      (let [sv-keyword (make-dm-sv-name dm-name dm-access-point)]
        (add-watch (get-sv-atom sv-keyword) :read-sv
                   (fn [_key _ref old-value new-value]
                     (when-not (= old-value new-value)
                       (reset! (dm-access-point dm-spec) (dissoc new-value :timestamp)))))))))

(defn disconnect-dms-from-statevars [dm-ns]
  (doseq [dm-name (dms/list-decision-modules dm-ns)]
    (let [dm-spec (dms/get-dm-from-name dm-name dm-ns)]
      (doseq [dm-access-point [:internal-decision :executed-decision :goal :decision-setup]]
        (let [sv-keyword (make-dm-sv-name dm-name dm-access-point)]
          (remove-watch (get-sv-atom sv-keyword) :read-sv))))))
          
(defn identify-dm-statevar [sv-keyword]
  (let [keyword-parts (s/split (name sv-keyword) #"-")]
    (when (and (= (first keyword-parts) "dm")
               (rest keyword-parts))
      (->> keyword-parts
          (remove (fn [elem] (some #(= elem %) ["dm" "executed" "decision" "setup" "goal" "internal"])))
          (clojure.string/join "-")
          keyword))))



;; derived statevars

(defn get-derived-svs [] (:derived-svs @statevar-categories))

(def activate-derived-sv-funs (atom {}))

(def deactivate-derived-sv-funs (atom {}))

(def derived-svs-default-values (atom {}))


(defn define-derived-statevar [sv-name default-value sv-to-watch update-fun] 
  (add-statevar sv-name :derived-svs default-value)
  (swap! activate-derived-sv-funs assoc sv-name
    (fn [] (add-watch (get-sv-atom sv-to-watch) :derive-sv update-fun)))
  (swap! deactivate-derived-sv-funs assoc sv-name
    (fn [] (remove-watch (get-sv-atom sv-to-watch) :derive-sv)))
  (swap! derived-svs-default-values assoc sv-name default-value))
  


(defn start-sv-derivations
  ([] (start-sv-derivations (get-derived-svs)))
  ([derived-svs]
   (doseq [sv derived-svs]
     ((sv @activate-derived-sv-funs)))))

(defn reset-derived-svs
  ([] (reset-derived-svs (get-derived-svs)))
  ([derived-svs]
   (doseq [sv derived-svs]
     (reset! (get-sv-atom sv) (sv @derived-svs-default-values)))))


(defn stop-sv-derivations
  ([] (stop-sv-derivations (get-derived-svs)))
  ([derived-svs]
   (doseq [sv derived-svs]
     ((sv @deactivate-derived-sv-funs)))))



(def max-elements-in-encoder-trajectory 100)

(define-derived-statevar :encoder-trajectory nil :encoders
  (fn [_key _ref _old-value new-value]
    (when new-value
      (let [old-trajectory-data (vec (:data (get-sv-value :encoder-trajectory)))
            new-trajectory-data (conj old-trajectory-data (dissoc new-value :timestamp))]
        (reset-sv-val :encoder-trajectory {:data
                                           (if (> (count new-trajectory-data) max-elements-in-encoder-trajectory)
                                             (subvec new-trajectory-data 1)
                                             new-trajectory-data)})))))

                                                           






