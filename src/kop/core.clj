;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.core
  (:require [hps.core :refer :all]
            [hps.decision-modules :refer :all]
            [kop.connection :as conn]
            [kop.connection.base :refer [stop]]
            [kop.statevars :as svs :refer [svv]]
            [kop.traces :as tc]
            [dynne.sampled-sound :as dynne]
            [hps.GUI.main :as guimain]
            [hps.GUI.structure :as guistruct]
            [kop.GUI.core :as gui]
            [kop.run :as run]
            kop.decision-modules)
  (:import [javax.sound.sampled AudioFormat AudioSystem AudioInputStream AudioFileFormat$Type]
           [java.io File]
           [java.lang Runtime System]
           [org.opencv.core Core CvType Mat]
           (nu.pattern OpenCV)))

;; ------------------------------------------------------------- ;;
;; -- START/STOP PROGRAM/GUI ----------------------------------- ;;
;; ------------------------------------------------------------- ;;  
  

(defn kopi-start []
  (run/connect-robot))
  
(defn record []
  (tc/start-sv-logging))
  
(defn record-stop []
  (tc/stop-sv-logging))
  
(defn kopi-stop []
  (when (bound? #'tc/stop-sv-logging)
    (tc/stop-sv-logging))
  (stop)
  (run/disconnect-robot))

  
(defn start-kopi-gui []  
  (guistruct/register-hps-mode "Kopernikus" gui/adjust-gui-for-kopi true)
  (guimain/start-hps-gui))
               
;; ------------------------------------------------------------- ;;
;; -- TESTS FOR SCRIPTED BEHAVIOR ------------------------------ ;;
;; ------------------------------------------------------------- ;; 
(defn look-around [] ; nochmal genauer ausprobieren: eine feste Zeit scheint besser auf verschiedenen Böden zu funktionieren als encoder ticks, 3s sind aber zu kurz für ganze Runde

  ;(let [completed-round (promise)
  ;      {enc-left-before :enc-left} (svv :encoders)]
    
  ;  (add-watch (svs/get-sv-atom :encoders) :complete-round
  ;    (fn [_key _ref _old {enc-left-after :enc-left}]
  ;      (when (> (- enc-left-after enc-left-before) 300) 
  ;        (deliver completed-round true))))
          
    (svs/reset-sv-val :base-command {:trans 0 :rot 30})       
    (future (dynne/play (dynne/read-sound "/home/kopernikus/hps/hps-kopernikus/resources/r2d2_sounds/1.mp3")))
  ;  @completed-round
    (Thread/sleep 3000)
    (svs/reset-sv-val :base-command {:trans 0 :rot 0}))
  ;  (remove-watch (svs/get-sv-atom :encoders) :complete-round)))
    
(defn nod []
  (svs/reset-sv-val :projector-command {:pos 10})
  (Thread/sleep 500)
  (svs/reset-sv-val :projector-command {:pos 25})
  (Thread/sleep 500)    
  (svs/reset-sv-val :projector-command {:pos 18}))
  
  
(defn refuse []
  (svs/reset-sv-val :base-command {:trans 0 :rot 30}) 
  (Thread/sleep 500)
  (svs/reset-sv-val :base-command {:trans 0 :rot -30}) 
  (Thread/sleep 500)
  (svs/reset-sv-val :base-command {:trans 0 :rot 30}) 
  (Thread/sleep 500)
  (svs/reset-sv-val :base-command {:trans 0 :rot 0})) 
  

(defn test-scripts []
  (kopi-start)
  (Thread/sleep 10000)
  (doseq [script [look-around]] ; refuse nod]]
    (record)
    (script)
    (record-stop)
    (Thread/sleep 1000))
  (kopi-stop))

  
;; ------------------------------------------------------------- ;;
;; -- SOUND ---------------------------------------------------- ;;
;; ------------------------------------------------------------- ;;
; source: https://stackoverflow.com/questions/21757757/sound-recording-not-working-in-java
(defn getAudioFormat []
  (let [sampleRate 16000
        sampleSizeInBits 16
        channels 1
        signed true
        bigEndian false]
   (AudioFormat. sampleRate sampleSizeInBits channels signed bigEndian)))
  
  
(defn record-sound 
  ([] (record-sound "/home/kopernikus/test.wav"))
  ([testfile]
   (let [audioFormat (getAudioFormat)
         targetDataLine (AudioSystem/getTargetDataLine audioFormat) 
         ais (AudioInputStream. targetDataLine)
         wav-file (File. testfile)]
     (.open targetDataLine)
     (.start targetDataLine)
     (println "volume: " (.getLevel targetDataLine))
     (println "controls: " (.getControls targetDataLine))
     (println "started capturing")
     (let [write-process (future (AudioSystem/write ais AudioFileFormat$Type/WAVE wav-file))]
       (Thread/sleep 3000)
       (println "stop sleeping, stop writing")
       (future-cancel write-process))
     (.stop targetDataLine)
     (.close targetDataLine))))
       
(defn play-and-record-sound []
  (future (record-sound))
  (future (dynne/play (dynne/read-sound "/home/kopernikus/hps/hps-kopernikus/resources/r2d2_sounds/1.mp3")))
  )
    
   


