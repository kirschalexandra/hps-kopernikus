;; Copyright 2018
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns kop.connection.ros
  (:require [kop.connection.RosNode :refer :all])
  (:import ;[java.net URI]
    [org.ros RosRun]
    [org.ros.node NodeConfiguration AbstractNodeMain DefaultNodeMainExecutor]))

;; ------------------------------------------------------------- ;;
;; -- ROS / CAMERA --------------------------------------------- ;;
;; ------------------------------------------------------------- ;;


;; usage of ros-test:
;; (def executor (ros-test)) -> you can examine executor object
;; (stop-ros) -> ends all ROS processes
(defn start-ros []
  ;(start-ros-components)
  (let [executor (DefaultNodeMainExecutor/newDefault)]
    (.execute executor (kop.connection.RosNode.) (NodeConfiguration/newPrivate))

    (defn stop-ros []
      (.shutdown executor))
      ;(stop-ros-components))
    executor))
